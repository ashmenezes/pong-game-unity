﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class countdown : MonoBehaviour {

    float timeLeft = 3.0f;

    void Update()
    {
        timeLeft -= Time.unscaledDeltaTime;
        GetComponent<Text>().text = "" + Mathf.Round(timeLeft);
        if (timeLeft < 0.5f)
        {
            GetComponent<Text>().text = "GO";
        }
        if(timeLeft < 0)
        {
            timeLeft = 3.0f;
            gameObject.SetActive(false);
            Time.timeScale = 1;
        }
    }
}
