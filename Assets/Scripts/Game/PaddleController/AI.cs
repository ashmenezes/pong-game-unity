﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour {

	public int difficulty;

	public float speed = 0.09f;

	public float easy = 0.09f; // 0.05
	public float medium = 0.14f; 
	public float hard = 0.19f;

    public GameObject ball;

	// Use this for initialization
	void Start () {
		if (DataScenes.gameMode == 1 || DataScenes.gameMode == 3)
			gameObject.GetComponent<AI> ().enabled = false;

		difficulty = DataScenes.AIdifficulty;
        if (difficulty == 0)
        {
            speed = easy;
            GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Paddles/Green");
        }
        else if (difficulty == 1)
        {
            speed = medium;
            GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Paddles/Yellow");
        }
        else if (difficulty == 2)
        {
            speed = hard;
            GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Paddles/Red");
        }
    }

	void Update() {
        ball = GameObject.Find("Ball(Clone)");
        if (ball && ball.transform.position.y > 0) 
		{
            Vector3 dest = new Vector3(ball.transform.position.x, transform.position.y, transform.position.z);
            if (dest.x < 13.5f && dest.x > -13.5f)
            {
                float d = dest.x - transform.position.x;
                if (d > 0)
                {
                    transform.position = Vector3.Lerp(transform.position, dest, /*Mathf.Min(d, 1.0f) **/ speed);
                }
                else if (d < 0)
                {
                    transform.position = Vector3.Lerp(transform.position, dest, /*Mathf.Min(-d, 1.0f) **/ speed);
                }
            }
		}
	}
}
