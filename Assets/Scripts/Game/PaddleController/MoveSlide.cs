﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveSlide : MonoBehaviour {

	public string axis;
	public float vel;
	//public bool player2 = false;

	private Vector2 pos;

	void Start() {
		if (PlayerPrefs.GetString ("paddle") == "") { // if playerprefs string is empty
			GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> ("Paddles/" + "Blue"); // Use default skin
			PlayerPrefs.SetString ("paddle", "Blue"); // assign default paddle as playerprefs string
		} else {
			GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Paddles/" + PlayerPrefs.GetString("paddle")); // Load sprite for paddle
		}
	}

	void FixedUpdate () 
	{
		Move ();
	}

	public void Move (){
        #if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER
        Vector3 mouseLocation = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(mouseLocation.x, transform.position.y, transform.position.z);
        #else
		if (Input.touchCount > 0)
            TouchControl(0);
        #endif
    }

    public void TouchControl(int touchnumber) {
        DebugManager.Log("touchcontrol");
		Touch touch = Input.GetTouch(touchnumber); // get first touch since touch count is greater than zero
		if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved) {
            Debug.LogError("touch!");
            if (touch.position.y < Screen.currentResolution.height / 2)
            {
                Vector3 touchedPos = Camera.main.ScreenToWorldPoint(touch.position); // get the touch position from the screen touch to world point
                Vector3 dest = new Vector3(touchedPos.x, transform.position.y, transform.position.z);
                transform.position = Vector3.Lerp(transform.position, dest, 1); // lerp and set the position of the current object to that of the touch, but smoothly over time.
            }
		}
	}

	public void Move (float v){
		vel = v;
		GetComponent<Rigidbody2D>().velocity = new Vector2(0, vel) * 30f;
	}

}
