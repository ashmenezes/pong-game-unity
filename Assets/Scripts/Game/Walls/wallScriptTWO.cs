﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wallScriptTWO : MonoBehaviour {

	void OnCollisionEnter2D (Collision2D hitInfo) 
	{
		if (hitInfo.gameObject.name == "Ball")
		{
			GameObject canvas = GameObject.Find("Canvas");
			GameManagerTWO gameManager = canvas.GetComponent<GameManagerTWO>();
			string wallName = transform.name;
			gameManager.Score(wallName);
			//hitInfo.gameObject.SendMessage ("BallReset", null, SendMessageOptions.RequireReceiver);
		}
	}
}