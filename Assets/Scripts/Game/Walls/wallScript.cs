﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wallScript : MonoBehaviour {

	void OnCollisionEnter2D (Collision2D hitInfo) 
	{
		if (hitInfo.gameObject.tag == "Ball")
		{
			GameObject canvas = GameObject.Find("Canvas");
			GameManager gameManager = canvas.GetComponent<GameManager>();
			string wallName = transform.name;
			gameManager.Score(wallName);
			//hitInfo.gameObject.SendMessage ("BallReset", null, SendMessageOptions.RequireReceiver);
		}
	}
}