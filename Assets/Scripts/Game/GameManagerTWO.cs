﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManagerTWO : MonoBehaviour {


	//public /*static*/ int LeftPlayer = 0;
	public /*static*/ int RightPlayer = 0;
	public /*static*/ Text leftScore;
	public /*static*/ Text rightScore;
	public /*static*/ GameObject Ball;

	void Start () {
		//leftScore = GameObject.Find("LeftScore").GetComponent<Text>();
		rightScore = GameObject.Find("RightScore").GetComponent<Text>();
		Ball = GameObject.Find("Ball");
	}

	public /*static*/ void Score (string wallName)
	{
		if (wallName == "LeftWall")
		{
			RightPlayer++;
			rightScore.text = "" + RightPlayer;
		}

		if (wallName == "RightWall")
		{
			rightScore.text = "SCORE " + RightPlayer;
			Ball.SendMessage ("BallStop", null, SendMessageOptions.RequireReceiver);
			Invoke ("RestartGame", 3);
		}
	}

	void Update()
	{
		/*if (LeftPlayer == 5)
		{
			leftScore.text = "YOU WIN";
			rightScore.text = "YOU LOSE";
			Ball.SendMessage ("BallStop", null, SendMessageOptions.RequireReceiver);
			Invoke ("RestartGame", 3);
		}

		if (RightPlayer == 5)
		{
			rightScore.text = "YOU WIN";
			leftScore.text = "YOU LOSE";
			Ball.SendMessage ("BallStop", null, SendMessageOptions.RequireReceiver);
			Invoke ("RestartGame", 3);
		}*/
	}

	void RestartGame()
	{
		Scene loadedLevel = SceneManager.GetActiveScene ();
		SceneManager.LoadScene (loadedLevel.buildIndex);
		//SceneManager.LoadScene("scene_main");
	}
}
