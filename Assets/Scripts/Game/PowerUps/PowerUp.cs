﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PowerUp : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Ball(Clone)")
        {
            string WhoHitLast = GameObject.Find("Ball(Clone)").GetComponent<BallMove>().WhoHitLast;
            //GaObject GotPowerUp = GameObject.Find(WhoHitLast);
            //Debug.Log("applying power to " + GotPowerUp);
            DoPowerUpAction(WhoHitLast);
            GameObject.Find("Canvas").GetComponent<ArcadeManager>().PowerUpsAmount--;
            GameObject.Find("Canvas").GetComponent<ArcadeManager>().SpawnPowerUp();
            if (DataScenes.gameMode == 2)
            {
                Destroy(gameObject);
            }else if (DataScenes.gameMode == 3 && PhotonNetwork.isMasterClient)
            {
                PhotonNetwork.Destroy(gameObject);
            }
        }
    }

    void DoPowerUpAction(string Received)
    {
        GameObject Recepient = GameObject.Find(Received);
        if (gameObject.name == "PowerUp(Clone)")
        {
            if (DataScenes.gameMode == 2)
                GrowPaddle(Received);
            else if (DataScenes.gameMode == 3 && PhotonNetwork.isMasterClient)
                GetComponent<PhotonView>().RpcSecure("GrowPaddle", PhotonTargets.All, true, Received);
        }
        if (gameObject.name == "PowerUpBad(Clone)")
        {
            if (DataScenes.gameMode == 2)
                ShrinkPaddle(Received);
            else if (DataScenes.gameMode == 3 && PhotonNetwork.isMasterClient)
                GetComponent<PhotonView>().RpcSecure("ShrinkPaddle", PhotonTargets.All, true, Received);
        }
        if (gameObject.name == "PowerUpWall(Clone)")
        {
            GameObject saveWall = Resources.Load<GameObject>("Arcade/SavePowerUp");
            if (DataScenes.gameMode == 2)
            {
                if (Recepient.transform.position.y > 0)
                    Instantiate(saveWall, new Vector3(0, Recepient.transform.position.y + 1, 0), Quaternion.Euler(new Vector3(0, 0, -90)));
                if (Recepient.transform.position.y < 0)
                    Instantiate(saveWall, new Vector3(0, Recepient.transform.position.y - 1, 0), Quaternion.Euler(new Vector3(0, 0, -90)));
            }else if (DataScenes.gameMode == 3 && PhotonNetwork.isMasterClient)
            {
                if (Recepient.transform.position.y > 0)
                    PhotonNetwork.Instantiate("Arcade/SavePowerUp", new Vector3(0, Recepient.transform.position.y + 1, 0), Quaternion.Euler(new Vector3(0, 0, -90)), 0);
                if (Recepient.transform.position.y < 0)
                    PhotonNetwork.Instantiate("Arcade/SavePowerUp", new Vector3(0, Recepient.transform.position.y - 1, 0), Quaternion.Euler(new Vector3(0, 0, -90)), 0);
            }
        }
        if (gameObject.name == "PowerUpBricks(Clone)")
        {

            if (DataScenes.gameMode == 2)
            {
                var BrickWalls = Resources.FindObjectsOfTypeAll<GameObject>().Where(obj => obj.name == "BrickWall(Clone)");
                foreach (GameObject todestroy in BrickWalls)
                {
                    Destroy(todestroy);
                }
                GameObject brickWall = Resources.Load<GameObject>("Arcade/BrickWall");
                Instantiate(brickWall, Vector3.zero, Quaternion.identity);
            }else if (DataScenes.gameMode == 3 && PhotonNetwork.isMasterClient)
            {
                var BrickWalls = Resources.FindObjectsOfTypeAll<GameObject>().Where(obj => obj.name == "BrickWall(Clone)");
                foreach (GameObject todestroy in BrickWalls)
                {
                    PhotonNetwork.Destroy(todestroy);
                }
                PhotonNetwork.Instantiate("Arcade/BrickWall", Vector3.zero, Quaternion.identity, 0);
            }
        }
        if (gameObject.name == "PowerUpBallGrow(Clone)")
        {
            if(DataScenes.gameMode == 3 && PhotonNetwork.isMasterClient)
            {
                GetComponent<PhotonView>().RpcSecure("BallGrow", PhotonTargets.All, true);
            }
            else if(DataScenes.gameMode == 2)
            {
                BallGrow();
            }
        }
        if (gameObject.name == "PowerUpBallShrink(Clone)")
        {
            if (DataScenes.gameMode == 3 && PhotonNetwork.isMasterClient)
            {
                GetComponent<PhotonView>().RpcSecure("ShrinkPaddle", PhotonTargets.All, true);
            }
            else if (DataScenes.gameMode == 2)
            {
                ShrinkPaddle();
            }
        }
    }

    [PunRPC]
    public void BallGrow()
    {
        //Ball grow stuff
        GameObject ballToGrow = GameObject.FindGameObjectWithTag("Ball");
        if (ballToGrow.transform.localScale.x < 2 && ballToGrow.transform.localScale.y < 2)
            ballToGrow.transform.localScale += new Vector3(0.5f, 0.5f, 0);
    }

    [PunRPC]
    public void ShrinkPaddle()
    {
        //Ball shrink stuff
        GameObject ballToShrink = GameObject.FindGameObjectWithTag("Ball");
        if (ballToShrink.transform.localScale.x > 1 && ballToShrink.transform.localScale.y > 1)
            ballToShrink.transform.localScale -= new Vector3(0.5f, 0.5f, 0);
    }

    [PunRPC]
    public void GrowPaddle(string Received)
    {
        if (DataScenes.gameMode == 3)
        {
            if (!PhotonNetwork.isMasterClient)
            {
                if (Received == "Player")
                {
                    GameObject Recepient = GameObject.Find("Opponent");
                    if (Recepient.transform.localScale.y < 1.3f)
                        Recepient.transform.localScale += new Vector3(0.0f, 0.3f, 0.0f);
                }
                else if (Received == "Opponent")
                {
                    GameObject Recepient = GameObject.Find("Player");
                    if (Recepient.transform.localScale.y < 1.3f)
                        Recepient.transform.localScale += new Vector3(0.0f, 0.3f, 0.0f);
                }
            }else if (PhotonNetwork.isMasterClient)
            {
                GameObject Recepient = GameObject.Find(Received);
                if (Recepient.transform.localScale.y < 1.3f)
                    Recepient.transform.localScale += new Vector3(0.0f, 0.3f, 0.0f);
            }
        }
        else
        {
            GameObject Recepient = GameObject.Find(Received);
            if (Recepient.transform.localScale.y < 1.3f)
                Recepient.transform.localScale += new Vector3(0.0f, 0.3f, 0.0f);
        }
    }

    [PunRPC]
    public void ShrinkPaddle(string Received)
    {
        if (DataScenes.gameMode == 3)
        {
            if (!PhotonNetwork.isMasterClient)
            {
                if (Received == "Player")
                {
                    GameObject Recepient = GameObject.Find("Opponent");
                    if (Recepient.transform.localScale.y > 0.7f)
                        Recepient.transform.localScale -= new Vector3(0.0f, 0.3f, 0.0f);
                }
                else if (Received == "Opponent")
                {
                    GameObject Recepient = GameObject.Find("Player");
                    if (Recepient.transform.localScale.y > 0.7f)
                        Recepient.transform.localScale -= new Vector3(0.0f, 0.3f, 0.0f);
                }
            }
            else if (PhotonNetwork.isMasterClient)
            {
                GameObject Recepient = GameObject.Find(Received);
                if (Recepient.transform.localScale.y > 0.7f)
                    Recepient.transform.localScale -= new Vector3(0.0f, 0.3f, 0.0f);
            }
        }
        else
        {
            GameObject Recepient = GameObject.Find(Received);
            if (Recepient.transform.localScale.y > 0.7f)
                Recepient.transform.localScale -= new Vector3(0.0f, 0.3f, 0.0f);
        }
    }
}
