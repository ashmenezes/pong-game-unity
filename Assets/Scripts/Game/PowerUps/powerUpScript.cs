﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class powerUpScript : MonoBehaviour {

    public Transform target;
    public Transform enemyWall;
    public Transform slideUnder;
    public GameObject newPower;
    public GameObject [] powerUp;


    void OnTriggerEnter2D(Collider2D collision)
    {
        int andPowerIndex = Random.Range(0, powerUp.Length);
        newPower = (GameObject)Instantiate(powerUp[andPowerIndex], GetComponent<Transform>().position, Quaternion.Euler(new Vector3(0, 0, 0)));
        newPower.GetComponent<Ability>().target = target;
        slideUnder.GetComponent<AbilityBehaviour>().goalSide = target;
        slideUnder.GetComponent<AbilityBehaviour>().enemyWall = enemyWall;
        gameObject.SetActive(false);
    }
}
