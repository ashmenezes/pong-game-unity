﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability : MonoBehaviour {

    public float speedtomove;
    public Transform target;
    public Vector3 endPoint;

    void Start()
    {
        float ypos = GetYLocation();
        endPoint = new Vector3(target.transform.position.x, ypos, 0);
    }

    float GetYLocation()
    {
        float y1 = target.transform.position.y - target.GetComponent<Renderer>().bounds.size.y / 2;
        float y2 = target.transform.position.y + target.GetComponent<Renderer>().bounds.size.y / 2;
        return Random.Range(y1, y2);
    }

    void Update()
    {
        if (transform.position != endPoint)
        {
            float step = speedtomove * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, endPoint, step);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name != "Ball")
        {
            Destroy(gameObject);
        }
    }
}
