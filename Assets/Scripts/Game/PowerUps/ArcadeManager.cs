﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcadeManager : MonoBehaviour {

    public GameObject[] PowerUps;
    public int PowerUpsAmount;

    public Vector3 DefaultScalePlayer;
    public Vector3 DefaultScaleOpponent;


    void Start()
    {
        GetDefaultValues();
        PowerUpsAmount = 0;
        if (DataScenes.gameMode == 2 || (DataScenes.gameMode == 3 && PhotonNetwork.isMasterClient))
        {
            SpawnPowerUp();
            SpawnPowerUp();
        }
    }

    GameObject RandomPowerup()
    {
        int PowerUpIndex = Random.Range(0, PowerUps.Length);
        return PowerUps[PowerUpIndex];
    }

    string RandomPowerupNet()
    {
        int PowerUpIndex = Random.Range(0, PowerUps.Length);
        return PowerUps[PowerUpIndex].name;
    }

    Vector3 RandomPosition()
    {
        Vector3 spawnPoint;
        spawnPoint.x = Random.Range(-7, 7);
        spawnPoint.y = Random.Range(-7, 7);
        spawnPoint.z = 0;
        return spawnPoint;
    }

    float RandomTime()
    {
        float spawnTime;
        spawnTime = 5 + Random.Range(0, 5);
        return spawnTime;
    }

    /*public void SpawnPowerUp(float delayTime)
    {
        Vector3 spawnPosition = RandomPosition();
        if (DataScenes.gameMode == 3 && PhotonNetwork.isMasterClient)
        {
            GameObject a = PhotonNetwork.Instantiate("Prefabs/arcade_scene/Game/PowerUps/" + RandomPowerupNet(), spawnPosition, Quaternion.identity, 0);
            
            DebugManager.Log(a.name + ": " + a.GetComponent<PhotonView>().viewID);
        }
        else if (DataScenes.gameMode == 2)
        {
            GameObject powerup = Instantiate(RandomPowerup(), spawnPosition, Quaternion.identity);

        }
    }*/

    IEnumerator PowerUpED(GameObject a, float delayTime)
    {
        a.GetComponent<SpriteRenderer>().enabled = false;
        a.GetComponent<BoxCollider2D>().enabled = false;
        yield return new WaitForSeconds(delayTime);
        if (a)
        {
            a.GetComponent<SpriteRenderer>().enabled = true;
            a.GetComponent<BoxCollider2D>().enabled = true;
            a.transform.GetChild(0).gameObject.SetActive(false);
        }
    }

    [PunRPC]
    public void powerupCallback(int a, float delayTime)
    {
        GameObject power = PhotonView.Find(a).gameObject;
        StartCoroutine(PowerUpED(power, delayTime));
    }

    public void SpawnPowerUp()
    {
        if (PowerUpsAmount < 2)
        {
            float time = RandomTime();
            Vector3 spawnPosition = RandomPosition();
            if (DataScenes.gameMode == 3 && PhotonNetwork.isMasterClient)
            {
                GameObject a = PhotonNetwork.Instantiate("Prefabs/arcade_scene/Game/PowerUps/" + RandomPowerupNet(), spawnPosition, Quaternion.identity, 0);
                GetComponent<PhotonView>().RpcSecure("powerupCallback", PhotonTargets.All, true, a.GetComponent<PhotonView>().viewID, time);
                DebugManager.Log(a.name + ": " + a.GetComponent<PhotonView>().viewID);
            }
            else if (DataScenes.gameMode == 2)
            {
                GameObject powerup = Instantiate(RandomPowerup(), spawnPosition, Quaternion.identity);
                StartCoroutine(PowerUpED(powerup, time));
            }
            PowerUpsAmount++;
        }
    }

    void GetDefaultValues()
    {
        DefaultScalePlayer = GameObject.Find("Player").transform.localScale;
        DefaultScaleOpponent = GameObject.Find("Opponent").transform.localScale;
    }

    [PunRPC]
    public void ResetPowerUps()
    {
        GameObject.Find("Player").transform.localScale = DefaultScalePlayer;
        GameObject.Find("Opponent").transform.localScale = DefaultScaleOpponent;
        GameObject[] powers = GameObject.FindGameObjectsWithTag("PowerUp");
        foreach (GameObject todestroy in powers)
        {
            if (DataScenes.gameMode == 2)
            {
                Destroy(todestroy);
            }
            else if (DataScenes.gameMode == 3 && PhotonNetwork.isMasterClient)
            {
                PhotonNetwork.Destroy(todestroy);
            }
        }
        if (DataScenes.gameMode == 2 || (DataScenes.gameMode == 3 && PhotonNetwork.isMasterClient))
        {
            PowerUpsAmount = 0;
            SpawnPowerUp();
            SpawnPowerUp();
        }
    }

}
