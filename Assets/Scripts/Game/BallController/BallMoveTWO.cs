﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMoveTWO : MonoBehaviour {

	public float speed;
	private Rigidbody2D rigidBody2D;
	public /*static*/ GameObject countDown;

	void Start()
	{
		GetComponent<Renderer>().enabled = false;
		countDown = GameObject.Find ("Countdown");
		//countDown.SetActive(false);
		rigidBody2D = GetComponent<Rigidbody2D> ();
		Invoke ("BallGo", 4.5f);
		StartCoroutine(Waitto());
	}

	void BallGo()
	{
		rigidBody2D.velocity = Vector2.right * speed;
	}

	float hitFactor(Vector2 ballPos, Vector2 racketPos, float racketHeight) 
	{
		return (ballPos.y - racketPos.y) / racketHeight;
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.name == "SlideLeft")
		{
			float y = hitFactor(transform.position, col.transform.position, col.collider.bounds.size.y);

			Vector2 dir = new Vector2(1, y).normalized;

			GetComponent<Rigidbody2D>().velocity = dir * speed;
			speed += 2;
		}

		if (col.gameObject.name == "RightWall")
		{
			float y = hitFactor(transform.position, col.transform.position, col.collider.bounds.size.y);

			Vector2 dir = new Vector2(-1, y).normalized;

			GetComponent<Rigidbody2D>().velocity = dir * speed;
			speed += 2;
		}
	}

	void BallReset()
	{
		BallStop ();
		//countDown.SetActive(true);
		//StartCoroutine(Waitto());
		Invoke ("BallGo", 1);
	}

	IEnumerator Waitto()
	{
		yield return new WaitForSeconds(4);
		countDown.SetActive(false);
		GetComponent<Renderer>().enabled = true;
	}

	void BallStop()
	{
		//countDown.SetActive(false);		
		rigidBody2D.velocity = Vector2.zero;
		transform.position = Vector2.zero;
	}
}
