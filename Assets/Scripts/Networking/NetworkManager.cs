﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using ExitGames.Client.Photon;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Reflection;

public class NetworkManager : MonoBehaviour {

    #region CLASS DEFINITIONS
    public class RoomSave
    {
        public string roomName;
        public RoomOptions roomOptions;
    }

    [System.Serializable]
    public class Rank : System.Object
    {
        [SerializeField] protected string playerName = "";  // "" = default
        [SerializeField] protected int playerId = 0;        // 0 = default
        [SerializeField] protected int rankNo = 0;          // 0 = default
        [SerializeField] protected int result = 0;          // 0 = default

        public Rank(string playerName, int playerId, int rankNo, int result)
        {
            this.playerName = playerName;   // nickname
            this.playerId = playerId;
            this.rankNo = rankNo;
            this.result = result;   // 0, 1, 2
        }

        [System.Xml.Serialization.XmlElement(IsNullable = true)]
        public string PlayerName
        {
            get { return this.playerName; }
            set { playerName = value; }
        }
        public int PlayerID
        {
            get { return this.playerId; }
            set { playerId = value; }
        }
        public int RankNumber
        {
            get { return this.rankNo; }
            set { rankNo = value; }
        }
        public int Result
        {
            get { return this.result; }
            set { result = value; }
        }

        public static byte[] SerializeRankArray(object customObject)
        {
            return DataConverter.SerializeRankArray((Rank[])customObject);
        }

        public static object DeserializeRankArray(byte[] bytes)
        {
            return DataConverter.DeserializeRankArray(bytes);
        }

        public static byte[] SerializeRank(object customObject)
        {
            return DataConverter.SerializeRank((Rank)customObject);
        }

        public static object DeserializeRank(byte[] bytes)
        {
            return DataConverter.DeserializeRank(bytes);
        }
    }

    public static class DataConverter
    {
        public static byte[] SerializeRankArray(Rank[] objectToSerialize)
        {
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream mStream = new MemoryStream();
            bf.Serialize(mStream, objectToSerialize);
            mStream.Position = 0;
            return mStream.ToArray();
        }

        public static Rank[] DeserializeRankArray(byte[] dataStream)
        {
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream mStream = new MemoryStream(dataStream);
            mStream.Position = 0;
            bf.Binder = new VersionFixer();
            Rank[] o = (Rank[])bf.Deserialize(mStream);
            return o;
        }

        public static byte[] SerializeRank(Rank objectToSerialize)
        {
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream mStream = new MemoryStream();
            bf.Serialize(mStream, objectToSerialize);
            mStream.Position = 0;
            return mStream.ToArray();
        }

        public static Rank DeserializeRank(byte[] dataStream)
        {
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream mStream = new MemoryStream(dataStream);
            mStream.Position = 0;
            bf.Binder = new VersionFixer();
            Rank o = (Rank)bf.Deserialize(mStream);
            return o;
        }

        sealed class VersionFixer : SerializationBinder
        {
            public override Type BindToType(string assemblyName, string typeName)
            {
                Type typeToDeserialize = null;

                // For each assemblyName/typeName that you want to deserialize to
                // a different type, set typeToDeserialize to the desired type.
                String assemVer1 = Assembly.GetExecutingAssembly().FullName;
                if (assemblyName != assemVer1)
                {
                    // To use a type from a different assembly version, 
                    // change the version number.
                    // To do this, uncomment the following line of code.
                    assemblyName = assemVer1;
                    // To use a different type from the same assembly, 
                    // change the type name.
                }
                // The following line of code returns the type.
                typeToDeserialize = Type.GetType(String.Format("{0}, {1}", typeName,
                assemblyName));
                return typeToDeserialize;
            }
        }
    }
    #endregion

    #region VARIABLES
    private string version = "v0.1";
    public RoomOptions roomOptions;

    public string MasterPlayerPrefabName;
    public string ClientPlayerPrefabName;
    public Transform playerSpawnPoint;
    public Transform cloneSpawnPoint;
    public int numberOfRooms;
    public GameObject connectingPanel;
    public GameObject opponentLeftPanel;
    public GameObject disconnectingPanel;

    [Header("Tournament GameObjects")]
    public GameObject tournyPanel;
    public Sprite yesSprite;
    public Sprite noSprite;

    // Tournament variables
    RoomInfo[] roomsList;
    bool receivedRoomList = false;
    int retryCounter = 0;
    RoomOptions tournyRoom;
    RoomOptions tournyDupRoom;
    RoomSave roomSave;
    Rank[] rankings = new Rank[7];
    int highestRankCount = 0;
    string opponent;
    int updateCount = 0;
    byte myCustomTypeCode;
    int myPlayerId = 0;
    bool allowDisconnectHandling = true;
    /// <summary>
    /// 0: default,
    /// 1: create room,
    /// 2: join room
    /// </summary>
    int busy = 0;
    #endregion

    // GUIDE FOR USING CUSTOM ROOM PROPERTY "lvl" IN TOURNAMENT MODE
    // lvl = 0: First match in first room hasn't started yet, or
    //          First match in second room hasn't started yet.
    // lvl = 1: First match in first room has started/is ongoing, or
    //          First match in second room has started/is ongoing.
    // lvl = 2: Second match in second room hasn't started yet.
    // lvl = 3: Second match in second room has started/is ongoing.
    // lvl = 4: Second match in second room has ended.
    void Start() {
        DebugManager.Log("NetworkManager Start");

        hideAllPopUpPanels();
        showConnecting(true);
        PhotonNetwork.ConnectUsingSettings(version);
        PhotonNetwork.autoCleanUpPlayerObjects = true;
        PhotonNetwork.playerName = PlayerPrefs.GetString("username");

        roomOptions = new RoomOptions() { PublishUserId = true, IsVisible = true, MaxPlayers = 2 };

        // Create tournyRoom room type
        tournyRoom = new RoomOptions() { PublishUserId = true, IsVisible = true, MaxPlayers = 4 };
        tournyRoom.CustomRoomPropertiesForLobby = new string[] { "lvl" };
        tournyRoom.CustomRoomProperties = new ExitGames.Client.Photon.Hashtable() {
            { "lvl", 0 }
        };

        tournyDupRoom = new RoomOptions() { PublishUserId = true, IsVisible = false, MaxPlayers = 2 };
        tournyDupRoom.CustomRoomPropertiesForLobby = new string[] { "lvl", "tournyRoomName" };
        tournyDupRoom.CustomRoomProperties = new ExitGames.Client.Photon.Hashtable() {
            { "lvl", 0 },
            { "tournyRoomName", ""},
        };

        roomSave = new RoomSave();
    }

    #region PHOTON METHODS
    void OnConnectedToMaster()
    {
        DebugManager.updateState("OnConnectedToMaster()");
        PhotonNetwork.JoinLobby(TypedLobby.Default);
    }

    void OnJoinedLobby() {
        DebugManager.updateState("OnJoinedLobby()");

        if (DataScenes.gameMode == 4) {
            if (busy == 0)
            {
                // Register Rank as new type
                PhotonPeer.RegisterType(typeof(Rank), (byte)'R', Rank.SerializeRankArray, Rank.DeserializeRankArray);
                PhotonPeer.RegisterType(typeof(Rank), (byte)'S', Rank.SerializeRank, Rank.DeserializeRank);

                // Wait for list of rooms
                StartCoroutine("FindTournyRoom");
            }
            else if (busy == 1)
                PhotonNetwork.CreateRoom(roomSave.roomName, roomSave.roomOptions, TypedLobby.Default);
            else if (busy == 2)
                PhotonNetwork.JoinRoom(roomSave.roomName);
        }
        else
            PhotonNetwork.JoinRandomRoom();
    }

    void OnJoinedRoom()
    {
        DebugManager.updateState("OnJoinedRoom()");
        if (DataScenes.gameMode == 4)   // if in tourny mode
        {
            connectingPanel.SetActive(false);
            tournyPanel.SetActive(true);
            tournyPanel.transform.Find("Status").GetComponent<Text>().text = "Waiting for other players.";

            if (PhotonNetwork.room.MaxPlayers == 4)    // if room is not dup
            {
                if ((int)PhotonNetwork.room.CustomProperties["lvl"] == 0)   // if game hasn't started yet
                {
                    for (int i = 0; i < rankings.Length; i++)
                    {
                        rankings[i] = new Rank("", 0, 0, 0);
                    }

                    // Update rankings lists for all players
                    if (PhotonNetwork.room.PlayerCount < 2)    // if the first player in the tourny
                    {
                        // increment rankCount
                        highestRankCount += 1;

                        // Create and add new rank to ranking list
                        myPlayerId = PhotonNetwork.player.ID;
                        rankings[highestRankCount - 1] = new Rank(PhotonNetwork.player.NickName, myPlayerId, highestRankCount, 0);

                        updateRankChart();

                        StartCoroutine(startFirstRoomFirstGame());
                    }
                    else if (PhotonNetwork.room.PlayerCount >= 2)    // if there are more than 1 player
                    {
                        // Update other players ranking info, and update their ranking chart
                        GetComponent<PhotonView>().RpcSecure("updateMyRankListAll", PhotonTargets.Others, true, PhotonNetwork.player.UserId);
                    }
                }
                else if ((int)PhotonNetwork.room.CustomProperties["lvl"] == 1)     // if first room's game is on-going
                {
                    // Update other players ranking info, and update their ranking chart
                    GetComponent<PhotonView>().RpcSecure("updateMyRankListAll", PhotonTargets.Others, true, PhotonNetwork.player.UserId);
                }
                else if ((int)PhotonNetwork.room.CustomProperties["lvl"] == 2)    // if first room's game is done
                {
                    DebugManager.Log("Room 1 lvl " + (string)PhotonNetwork.room.CustomProperties["lvl"]);
                    StartCoroutine(startFirstRoomSecondGame());

                    // Stop players from joining the room
                    PhotonNetwork.room.IsOpen = false;
                }
            }
            else if (PhotonNetwork.room.MaxPlayers == 2)    // if room is dup
            {
                if (PhotonNetwork.room.PlayerCount == 1)    // first player in the room
                {
                    StartCoroutine(startSecondRoomFirstGame());
                }
                else if (PhotonNetwork.room.PlayerCount > 1)     // if all players have arrived
                {

                }
            }
        }
        else
        {
            // Do regular online stuff
            int numberOfplayers = PhotonNetwork.playerList.Length;
            if (numberOfplayers > 1)
            {
                GameObject player = PhotonNetwork.Instantiate(ClientPlayerPrefabName,
                    cloneSpawnPoint.position,
                    cloneSpawnPoint.rotation,
                    0);
                //player.GetComponent<MoveSlide>().enabled = true;
                GameObject.Find("Canvas").GetComponent<GameManager>().countDown.SetActive(true);
                GetComponent<PhotonView>().RpcSecure("showConnecting", PhotonTargets.All, true, false);
            }
            else
            {
                PhotonNetwork.Instantiate(MasterPlayerPrefabName,
                    playerSpawnPoint.position,
                    playerSpawnPoint.rotation,
                    0);
                StartCoroutine(WaitInRoom());
            }
        }
    }

    void OnPhotonRandomJoinFailed()
    {
        DebugManager.updateState("OnPhotonRandomJoinFailed()");
        CreateRandomRoom();
    }

    void OnPhotonJoinRoomFailed()
    {
        DebugManager.Log("OnPhotonJoinRoomFailed()");
        if (DataScenes.gameMode == 4)
        {
            if (retryCounter < 3)
            {
                PhotonNetwork.JoinRoom(roomSave.roomName);
                retryCounter++;
                DebugManager.Log("Join Tourny Retry #" + retryCounter + " (" + (3 - retryCounter) + ") tries left");
            }
            else
            {
                DebugManager.Log("Join Tourny Retry Limit Reached (3). Exiting to Main Menu.");
                playerDisconnect();
                SceneManager.LoadScene("main_menu");
            }
        }
    }

    void OnPhotonCreateRoomFailed()
    {
        DebugManager.updateState("OnPhotonCreateRoomFailed()");
        if (DataScenes.gameMode == 4)
        {
            if (retryCounter < 3)
            {                                          // NOTE: Does not distinguish between creating a new tourny or a duplicate room
                retryCounter++;
                DebugManager.Log("Create Tourny Retry " + retryCounter);
                PhotonNetwork.CreateRoom(roomSave.roomName, roomSave.roomOptions, TypedLobby.Default);
            }
            else
            {
                DebugManager.Log("Create Tourny Retry Limit Reached (3). Exiting to Main Menu.");
                playerDisconnect();
                SceneManager.LoadScene("main_menu");
            }
        }
        else
        {
            PhotonNetwork.JoinRandomRoom();
        }
    }

    void OnReceivedRoomListUpdate()
    {
        roomsList = PhotonNetwork.GetRoomList();
        receivedRoomList = true;
    }

    public void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        DebugManager.Log("OnPhotonPlayerDisconnected()");
        if (DataScenes.gameMode == 4)
        {
            if (PhotonNetwork.room.MaxPlayers == 2)   // if dup room
            {
                if ((int)PhotonNetwork.room.CustomProperties["lvl"] == 0 || (int)PhotonNetwork.room.CustomProperties["lvl"] == 1)
                {
                    /// Declare the other player (the one who DIDNT disconnect) as the winner,
                    /// and send him to the first room for the next match
                    /// 
                    foreach (PhotonPlayer p in PhotonNetwork.playerList)
                    {
                        if (p.UserId != otherPlayer.UserId)
                        {
                            tournyEndGame(p);
                            break;
                        }
                    }
                }
            }
            else if ((int)PhotonNetwork.room.MaxPlayers == 4)     // if NOT dup room
            {
                if ((int)PhotonNetwork.room.CustomProperties["lvl"] == 0 && allowDisconnectHandling)
                {
                    // Disconnect all players
                    GetComponent<PhotonView>().RpcSecure("playerDisconnect", PhotonTargets.All, true);
                }
                else if ((int)PhotonNetwork.room.CustomProperties["lvl"] == 1)
                {
                    if (otherPlayer.NickName == rankings[0].PlayerName || otherPlayer.UserId == rankings[1].PlayerName)
                    {
                        if (PhotonNetwork.room.PlayerCount == 2)
                        {
                            /// if there are only 2 players playing the first game in the first room
                            /// handle disconnects like normal
                            /// Wait for the next player to arrive 
                            /// 

                            foreach (PhotonPlayer p in PhotonNetwork.playerList)
                                if (p.UserId != otherPlayer.UserId)
                                    tournyEndGame(otherPlayer);
                        }
                        else if (PhotonNetwork.room.PlayerCount > 2)
                        {
                            /// if Rank 1 or 2 disconnected when Rank 6 is in the room.
                            /// Find the playerid of the player who isnt disconnected and who isnt Rank 6,
                            /// Set them as the winner
                            /// update everyone

                            // Find Rank 6
                            Rank rank6 = new Rank("", 0, 0, 0);
                            foreach (Rank r in rankings)
                            {
                                if (r.RankNumber == 6)
                                {
                                    rank6 = r;
                                    break;
                                }
                            }

                            // Find id of winner player
                            foreach (PhotonPlayer p in PhotonNetwork.playerList)
                            {
                                if (p.ID != otherPlayer.ID && p.UserId != rank6.PlayerName)
                                {
                                    tournyEndGame(p);
                                    break;
                                }
                            }

                        }
                    }
                    else if (otherPlayer.UserId == rankings[4].PlayerName && rankings[4].RankNumber == 6)
                    {
                        /// if the player who joined from the dup room disconnected,
                        /// set Rank 6 as the loser
                        /// add Rank 5 and set as the winner
                        /// update everyone

                        // set Rank 6 as the loser
                        foreach (Rank r in rankings)
                        {
                            if (r.RankNumber == 6)
                            {
                                r.Result = 2;
                                break;
                            }
                        }

                        // add Rank 5 and set as the winner
                        rankings[4] = new Rank("", 0, 5, 2);

                        // Update other players ranking info, and update their ranking chart
                        GetComponent<PhotonView>().RpcSecure("publishUpdatedRankList", PhotonTargets.Others, true, rankings);
                    }
                }
                else if ((int)PhotonNetwork.room.CustomProperties["lvl"] == 2)
                {
                    // If the match has started, make the player (the one who DIDNT disconnect, and NOT the one waiting to play the 2nd match) as the winner,

                }
                else if ((int)PhotonNetwork.room.CustomProperties["lvl"] == 3)
                {

                }
                else if ((int)PhotonNetwork.room.CustomProperties["lvl"] == 4)
                {

                }
            }
        }
        else if (DataScenes.gameMode == 2 || DataScenes.gameMode == 3)
        {
            playerDisconnect();
        }
    }
    #endregion
    
    #region CUSTOM METHODS
    IEnumerator FindTournyRoom()
    {
        DebugManager.Log("FindTournyRoom()");
        while (!receivedRoomList) {      // Wait for OnRecieveRoomListUpdate() to give list of rooms
            yield return new WaitForSeconds(1);
        }

        foreach (RoomInfo room in roomsList)    // For each room in room list
        {
            if (room.MaxPlayers == 4)   // if room is tournament mode
            {
                if (room.PlayerCount < room.MaxPlayers)     // if room isn't full
                {
                    if ((int)room.CustomProperties["lvl"] == 0)    // if match hasn't started
                    {
                        roomSave.roomName = room.Name;  // save room information

                        PhotonNetwork.JoinRoom(roomSave.roomName);
                        yield break;
                    }
                }
            }
        }
        createAndJoinNewTourny();
    }

    void createAndJoinNewTourny() {                                              // Create new tournament room
        DebugManager.Log("createAndJoinNewTourny()");

        string verve = "";                                                              // Create random string for room name
        var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".ToCharArray();
        while (verve.Length < 15)
            verve += characters[UnityEngine.Random.Range(0, characters.Length)];

        roomSave.roomOptions = tournyRoom;
        PhotonNetwork.CreateRoom(roomSave.roomName, roomSave.roomOptions, TypedLobby.Default);        // Create and join room
    }

    public void CreateRandomRoom() {
        DebugManager.updateState("CreateRandomRoom()");
        PhotonNetwork.CreateRoom("", roomOptions, TypedLobby.Default);
    }

    [PunRPC]
    void updateAllowDisconnectHandling(bool value)
    {
        DebugManager.Log("updateAllowDisconnectHandling()");
        allowDisconnectHandling = value;
    }

    /// <summary>
    /// Tells all players to update the other players with ranking information.
    /// </summary>
    [PunRPC]
    void updateMyRankListAll(string userID)
    {
        DebugManager.Log("updateMyRankListAll()");
        byte[] data = Rank.SerializeRankArray(rankings);
        GetComponent<PhotonView>().RpcSecure("updateMyRankList", PhotonTargets.Others, true, data, userID, PhotonNetwork.playerName);
    }

    /// <summary>
    /// Called by other players to update the current player's ranking list, then adds a new rank to the list.
    /// </summary>
    /// <param name="data">List of updated rankings</param>
    /// <param name="userID">Target user</param>
    [PunRPC]
    void updateMyRankList(byte[] data, string userID, string sender)
    {
        if (userID == PhotonNetwork.player.UserId) // if player is the right recipient
        {
            DebugManager.Log("updateMyRankList()");

            Rank[] rankingsUpdate = (Rank[])Rank.DeserializeRankArray(data);

            int maxRankNo = 0;

            // Compare and update rankings list
            for (int i = 0; i < rankings.Length; i++)
            {
                // if uninstantiated rank
                if (rankings[i].RankNumber == 0 && rankings[i].PlayerName == "")
                {
                    rankings[i] = new Rank(rankingsUpdate[i].PlayerName, rankingsUpdate[i].PlayerID, rankingsUpdate[i].RankNumber, rankingsUpdate[i].Result);
                    maxRankNo = maxRankNo < rankings[i].RankNumber ? rankings[i].RankNumber : maxRankNo;
                }
            }

            // increment rankCount
            highestRankCount = maxRankNo > highestRankCount ? maxRankNo+1 : highestRankCount;

            if (rankings[highestRankCount - 1].RankNumber == 0 || rankings[highestRankCount - 1].PlayerName == "")
            {
                // Add my new rank to list
                rankings[highestRankCount - 1] = new Rank(PhotonNetwork.player.NickName, myPlayerId, highestRankCount, 0);
            }

            // Update Rankings Chart
            updateRankChart();

            // Sends updated ranking list to all other players
            byte[] data1 = Rank.SerializeRankArray(rankings);
            GetComponent<PhotonView>().RpcSecure("publishUpdatedRankList", PhotonTargets.Others, true, data1);
        }
    }

    /// <summary>
    /// Updates other lists with new updated ranking
    /// </summary>
    /// <param name="data">New ranking list update</param>
    [PunRPC]
    void publishUpdatedRankList(byte[] data)
    {
        DebugManager.Log("publishUpdatedRankList()");
        Rank[] rankingsUpdate = (Rank[])Rank.DeserializeRankArray(data);

        int maxRankNo = 0;

        // Compare and update rankings list
        for (int i = 0; i < rankings.Length; i++)
        {
            // if uninstantiated rank
            if (rankings[i].RankNumber == 0 && rankings[i].PlayerName == "")
            {
                rankings[i] = new Rank(rankingsUpdate[i].PlayerName, rankingsUpdate[i].PlayerID, rankingsUpdate[i].RankNumber, rankingsUpdate[i].Result);
                maxRankNo = maxRankNo < rankings[i].RankNumber ? rankings[i].RankNumber : maxRankNo;
            }
        }

        // Update Rankings Chart
        updateRankChart();
    }

    /// <summary>
    /// Update player's (self) ranking chart
    /// </summary>
    void updateRankChart()
    {
        //DebugManager.Log("updateRankChart()");
        for (int i = 0; i < rankings.Length; i++)
        {
            if (rankings[i] != null)
            {
                GameObject tournyRankPanel = tournyPanel.transform.Find("TournamentRankPanel").gameObject;

                if (rankings[i].RankNumber != 0)
                {
                    Text t = tournyRankPanel.transform.Find("Rank" + rankings[i].RankNumber).GetComponent<Text>();
                    t.text = rankings[i].PlayerName;

                    Image im = t.transform.GetChild(0).GetComponent<Image>();

                    if (rankings[i].Result == 1)
                    {
                        im.sprite = yesSprite;
                        im.color = new Color(255, 255, 255, 255);
                    }
                    else if (rankings[i].Result == 2)
                    {
                        im.sprite = noSprite;
                        im.color = new Color(255, 255, 255, 255);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Sends players to create and join the duplicate room
    /// </summary>
    /// <returns></returns>
    IEnumerator startFirstRoomFirstGame()
    {
        DebugManager.Log("SendPlayersToDuplicateRoom()");

        while ((int)PhotonNetwork.room.PlayerCount < 3) // 4
        {
            yield return new WaitForSeconds(1);
        }

        DebugManager.Log("SendPlayersToDuplicateRoom() PlayerCount == 3");

        // Stop players from seeing this room in lobby's room list
        PhotonNetwork.room.IsVisible = false;

        // disable disconnect handling
        GetComponent<PhotonView>().RpcSecure("updateAllowDisconnectHandling", PhotonTargets.All, true, false);
        yield return new WaitForSeconds(3);

        // Store first and second player to be sent to the other tourny roomk
        string firstplayer = PhotonNetwork.playerList[0].NickName;
        string secondplayer = PhotonNetwork.playerList[1].NickName;

        // Create random string for room namej
        string verve = "";
        var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".ToCharArray();
        while (verve.Length < 15)
            verve += characters[UnityEngine.Random.Range(0, characters.Length)];

        yield return new WaitForSeconds(1);

        // Send first player to create room
        GetComponent<PhotonView>().RpcSecure("SendPlayerToCreateDuplicateTournyRoom", PhotonTargets.All, true, verve, firstplayer);
        yield return new WaitForSeconds(3);

        // Send second player to join that room
        GetComponent<PhotonView>().RpcSecure("SendPlayerToJoinDuplicateTournyRoom", PhotonTargets.All, true, verve, secondplayer);
        yield return new WaitForSeconds(3);

        // Start the game
        PhotonNetwork.room.CustomProperties["lvl"] = 1;
        GetComponent<PhotonView>().RpcSecure("updateAllowDisconnectHandling", PhotonTargets.All, true, true);

        PhotonNetwork.room.CustomProperties["lvl"] = 2;

        //DebugManager.Log("ROOM 1 GAME 1 START");
        //GetComponent<PhotonView>().RpcSecure("StartGame", PhotonTargets.All, true);
    }

    [PunRPC]
    void waitForGameToStart(string playerId, int roomNo, int gameNo)
    {
        if (PhotonNetwork.player.NickName != playerId)
            return;

        DebugManager.Log("waitForGameToStart(" + playerId + ", " + roomNo + ", " + gameNo + ")");
        if (roomNo == 1)
        {
            if (gameNo == 1)
                StartCoroutine(startFirstRoomFirstGame());
            else if (gameNo == 2)
                StartCoroutine(startFirstRoomSecondGame());
        }else if (roomNo == 2)
        {
            StartCoroutine(startSecondRoomFirstGame());
        }
    }

    /// <summary>
    /// Sends player to create duplicate room.
    /// </summary>
    /// <param name="name">Name of the duplicate room</param>
    /// <param name="playerId">ID of player who must create the room</param>
    [PunRPC]
    void SendPlayerToCreateDuplicateTournyRoom(string roomname, string playerId)
    {
        if (PhotonNetwork.player.NickName == playerId)    // if desired player
        {
            DebugManager.Log("SendPlayerToCreateDuplicateTournyRoom()");

            // Save current room information
            roomSave.roomName = roomname;
            roomSave.roomOptions = tournyDupRoom;

            // Make sure players in dup room can return to this room
            tournyDupRoom.CustomRoomProperties["tournyRoomName"] = PhotonNetwork.room.Name;

            PhotonNetwork.LeaveRoom();
            busy = 1;
            PhotonNetwork.JoinLobby(TypedLobby.Default);
        }
    }

    /// <summary>
    /// Sends player to join duplicate room.
    /// </summary>
    /// <param name="name">Name of the duplicate room</param>
    /// <param name="playerId">ID of the player who must join the room.</param>
    [PunRPC]
    void SendPlayerToJoinDuplicateTournyRoom(string roomname, string playerId)
    {
        if (PhotonNetwork.player.NickName == playerId)    // if desired player
        {
            DebugManager.Log("SendPlayerToJoinDuplicateTournyRoom()");
            // Save current room info
            roomSave.roomName = roomname;

            PhotonNetwork.LeaveRoom();
            busy = 2;
            PhotonNetwork.JoinLobby(TypedLobby.Default);
        }
    }

    IEnumerator startSecondRoomFirstGame()
    {
        DebugManager.Log("startSecondRoomFirstGame()");

        while ((int)PhotonNetwork.room.PlayerCount < 2)
        {
            yield return new WaitForSeconds(1);
        }

        GetComponent<PhotonView>().RpcSecure("updateAllowDisconnectHandling", PhotonTargets.All, true, true);
        PhotonNetwork.room.CustomProperties["lvl"] = 1; // Game has started!
        DebugManager.Log("ROOM 2 GAME 1 START");

        //byte[] data1 = DataConverter.SerializeRank(rankings[2]);
        //byte[] data2 = DataConverter.SerializeRank(rankings[3]);
        GetComponent<PhotonView>().RpcSecure("StartGame", PhotonTargets.All, true);
    }

    IEnumerator startFirstRoomSecondGame()
    {
        DebugManager.Log("startFirstRoomSecondGame()");

        while ((int)PhotonNetwork.room.PlayerCount < 3 && (int) PhotonNetwork.room.CustomProperties["lvl"] < 3)
        {
            yield return new WaitForSeconds(1);
        }

        GetComponent<PhotonView>().RpcSecure("updateAllowDisconnectHandling", PhotonTargets.All, true, true);
        PhotonNetwork.room.CustomProperties["lvl"] = 3; // Game has started!
        DebugManager.Log("ROOM 1 GAME 2 START");

        //byte[] data1 = DataConverter.SerializeRank(rankings[2]);
        //byte[] data2 = DataConverter.SerializeRank(rankings[3]);
        GetComponent<PhotonView>().RpcSecure("StartGame", PhotonTargets.All, true);
    }

    /// <summary>
    /// Starts the game by instantiating the paddles and ball.
    /// </summary>
    [PunRPC]
    void StartGame()
    {
        DebugManager.Log("StartGame()");
        GetComponent<PhotonView>().RpcSecure("updateAllowDisconnectHandling", PhotonTargets.All, true, true);

        // Get Opponent's Name
        int lvl = (int)PhotonNetwork.room.CustomProperties["lvl"];
        if (lvl == 1)
            if (PhotonNetwork.room.MaxPlayers == 4) {
                opponent = rankings[0].PlayerName == PhotonNetwork.playerName ? rankings[1].PlayerName : rankings[0].PlayerName;
                PhotonNetwork.room.CustomProperties["lvl"] = 2;
                return;
            }else if (PhotonNetwork.room.MaxPlayers == 2)
                opponent = rankings[2].PlayerName == PhotonNetwork.playerName ? rankings[3].PlayerName : rankings[2].PlayerName;
        else if (lvl == 3)
            opponent = rankings[4].PlayerName == PhotonNetwork.playerName ? rankings[5].PlayerName : rankings[4].PlayerName;

        if (PhotonNetwork.isMasterClient)
        {
            PhotonNetwork.Instantiate(MasterPlayerPrefabName,
                    playerSpawnPoint.position,
                    playerSpawnPoint.rotation,
                    0);
        }
        else
        {
            GameObject player = PhotonNetwork.Instantiate(ClientPlayerPrefabName,
                    cloneSpawnPoint.position,
                    cloneSpawnPoint.rotation,
                    0);
            //player.GetComponent<MoveSlide>().enabled = true;
            GameObject.Find("Canvas").GetComponent<GameManager>().countDown.SetActive(true);
            GetComponent<PhotonView>().RpcSecure("showConnecting", PhotonTargets.All, true, false);
        }

        tournyPanel.SetActive(false);
    }

    [PunRPC]
    void clearAllNetworkObjects()
    {
        PhotonNetwork.DestroyAll();
    }

    public IEnumerator tournyEndGame(PhotonPlayer winner = null) {
        tournyPanel.SetActive(true);

        DebugManager.Log("tournyEndGame()");

        if (winner == null)
            winner = PhotonNetwork.player;

        if ((int)PhotonNetwork.room.CustomProperties["lvl"] == 1)
        {
            if (PhotonNetwork.room.MaxPlayers == 4)     // if first room
            {
                // Add player ranking to rankings list
                rankings[4] = new Rank(winner.NickName, winner.ID, 5, 0);

                if (rankings[0].PlayerName == winner.NickName)
                {
                    rankings[0].Result = 1;
                    rankings[1].Result = 2;
                }
                else if (rankings[1].PlayerName == winner.NickName)
                {
                    rankings[0].Result = 2;
                    rankings[1].Result = 1;
                }

                PhotonNetwork.room.CustomProperties["lvl"] = 2;

                // Update other players with rankings
                byte[] data = Rank.SerializeRankArray(rankings);
                publishUpdatedRankList(data);

                // Set status text to waiting for other players
                tournyPanel.transform.Find("Status").GetComponent<Text>().text = "Waiting for other players";
            }
            else if (PhotonNetwork.room.MaxPlayers == 2)   // if second room
            {
                // find opponent's name
                PhotonPlayer opponentPlayer;
                foreach (PhotonPlayer p in PhotonNetwork.playerList)
                {
                    if (p.NickName != opponent)
                    {
                        opponent = p.NickName;
                        opponentPlayer = p;
                    }
                }

                // Find winner and loser in rankings and set their results
                if (rankings[0].PlayerName == winner.NickName)
                {
                    rankings[1].Result = 2;
                    rankings[0].Result = 1;
                }
                else if (rankings[1].PlayerName == winner.NickName)
                {
                    rankings[0].Result = 2;
                    rankings[1].Result = 1;
                }

                // Add winning player to rankings list
                rankings[4] = new Rank(winner.NickName, winner.ID, 5, 0);

                // update rankings list
                updateRankChart();
                tournyPanel.transform.Find("Status").GetComponent<Text>().text = "Waiting for other players";

                yield return new WaitForSeconds(5);

                // save current room info
                roomSave.roomName = (string)PhotonNetwork.room.CustomProperties["tournyRoomName"];

                DebugManager.Log("Log before leaving room");

                // Join room
                PhotonNetwork.LeaveRoom();
                busy = 2;
                PhotonNetwork.JoinLobby(TypedLobby.Default);

                tournyPanel.SetActive(false);
            }
        }
        else if ((int)PhotonNetwork.room.CustomProperties["lvl"] == 4)
        {
            // find loser's id
            string opponentId = "";
            foreach (PhotonPlayer p in PhotonNetwork.playerList)
            {
                if (p.UserId != winner.UserId)
                    opponentId = winner.UserId;
            }

            // Find winner and loser in rankings and set their results
            foreach (Rank r in rankings)
            {
                if (r.PlayerName == opponentId)
                    r.Result = 2;
                if (r.PlayerName == winner.NickName)
                    r.Result = 1;
            }

            // Add winning player to rankings list
            rankings[5] = new Rank(winner.NickName, winner.ID, 6, 1);

            // update rankings list
            updateRankChart();
            tournyPanel.SetActive(true);
            tournyPanel.transform.Find("Status").GetComponent<Text>().text = "Waiting for other players";
        }
    }

    IEnumerator WaitInRoom()
    {
        DebugManager.Log("WaitInRoom()");
        int limit = 10 + UnityEngine.Random.Range(0, 10);
        int timer = 0;
        bool interrupt = false;
        while (timer < limit)
        {
            if (PhotonNetwork.playerList.Length > 1)
            {
                interrupt = true;
                break;
            }
            yield return new WaitForSeconds(1);
            timer += 1;
        }
        if (interrupt)
            yield break;
        leaveRoomAndReconnect();
    }

    public void hideAllPopUpPanels()
    {
        showConnecting(false);
        opponentLeftPanel.SetActive(false);
        disconnectingPanel.SetActive(false);
    }

    [PunRPC]
    public void showConnecting(bool val)
    {
        connectingPanel.SetActive(val);
    }

    public void endGame()
    {
        DebugManager.Log("endGame()");
        GameObject.Find("Canvas").GetComponent<GameManager>().hasGameEnded = true;
        PhotonNetwork.DestroyAll();
        PhotonNetwork.Disconnect();
    }

    public void playAgain()
    {
        DebugManager.Log("playAgain()");
        hideAllPopUpPanels();
        connectingPanel.SetActive(true);
        GameObject.Find("GameOverPanel").SetActive(false);
        Scene s = SceneManager.GetActiveScene();
        SceneManager.LoadScene(s.buildIndex);
    }

    public void leaveRoomAndReconnect()
    {
        DebugManager.Log("leaveRoomAndReconnect");
        PhotonNetwork.DestroyAll();
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.LeaveLobby();
        PhotonNetwork.JoinLobby(TypedLobby.Default);
    }

    [PunRPC]
    public void playerDisconnect()
    {
        DebugManager.Log("playerDisconnect");
        hideAllPopUpPanels();
        disconnectingPanel.SetActive(true);
        PhotonNetwork.DestroyAll();
        PhotonNetwork.Disconnect();
        SceneManager.LoadScene("main_menu");
    }

    public void opponentDisconnect(PhotonPlayer otherPlayer)
    {
        DebugManager.Log("opponentDisconnect()");
        DebugManager.Log("opponentDisconnect(otherPlayer)");
        PhotonNetwork.DestroyAll();
        PhotonNetwork.Disconnect();
        StartCoroutine(ShowDisconnectMessage());
    }

    IEnumerator ShowDisconnectMessage()
    {
        DebugManager.Log("ShowDisconnectMessage()");
        opponentLeftPanel.SetActive(true);
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("main_menu");
    }
    #endregion 
}