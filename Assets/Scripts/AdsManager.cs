﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using GoogleMobileAds.Api;
using UnityEngine.SceneManagement;

public class AdsManager : MonoBehaviour {

    private BannerView bannerView;
    private InterstitialAd interstitial;
    private RewardBasedVideoAd rewardBasedVideo;

    public void Start()
    {
    #if UNITY_ANDROID
            string appId = "ca-app-pub-1398998864673726~2575876233";
    #elif UNITY_IPHONE
            string appId = "ca-app-pub-3940256099942544~1458002511";      
    #else
            string appId = "unexpected_platform";
    #endif

        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);
        if(!PlayerPrefs.HasKey("NoAds"))
        {
            if(SceneManager.GetActiveScene().name != "scene_main")
            {
                this.RequestBanner();
            }
            this.RequestInterstitial();
        }
        this.rewardBasedVideo = RewardBasedVideoAd.Instance;
        this.RequestRewardBasedVideo();
    }

    private void RequestRewardBasedVideo()
    {
    #if UNITY_ANDROID
        string adUnitId = "ca-app-pub-1398998864673726/7604499726";
    #elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-3940256099942544/1712485313";
    #else
        string adUnitId = "unexpected_platform";
    #endif

        // Create an empty ad request.
        //AdRequest request = new AdRequest.Builder().AddTestDevice("4CCA2E855C82B4E66105B9ADF02D698C").Build();
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded video ad with the request.
        this.rewardBasedVideo.LoadAd(request, adUnitId);
    }

    private void RequestBanner()
    {
    #if UNITY_ANDROID
        string adUnitId = "ca-app-pub-1398998864673726/1721959560";
    #elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-3940256099942544/2934735716";
    #else
        string adUnitId = "unexpected_platform";
    #endif

        
        if(!PlayerPrefs.HasKey("NoAds"))
        {
            // Create a 320x50 banner at the top of the screen.
            bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Top);

            // Create an empty ad request.
            AdRequest request = new AdRequest.Builder().Build();

            // Load the banner with the request.
            bannerView.LoadAd(request);
        }
    }

    private void RequestInterstitial()
    {
    #if UNITY_ANDROID
        string adUnitId = "ca-app-pub-1398998864673726/3213473353";
    #elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-3940256099942544/4411468910";
    #else
        string adUnitId = "unexpected_platform";
    #endif

    
        if(!PlayerPrefs.HasKey("NoAds"))
        {
            // Initialize an InterstitialAd.
            interstitial = new InterstitialAd(adUnitId);

            interstitial.OnAdClosed += HandleOnAdClosed;

            // Create an empty ad request.
            AdRequest request = new AdRequest.Builder().Build();
            // Load the interstitial with the request.
            interstitial.LoadAd(request);
        }
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdClosed event received");
    }

    public void ShowIntAd()
    {
        if (!PlayerPrefs.HasKey("NoAds"))
        {
            if (interstitial.IsLoaded())
            {
                interstitial.Show();
                interstitial.Destroy();
            }
        }
    }

    public void ShowRewAd()
    {
        if (rewardBasedVideo.IsLoaded())
        {
            rewardBasedVideo.Show();
        }
        RequestRewardBasedVideo();
    }

    public void DestroyAds()
    {
        if (!PlayerPrefs.HasKey("NoAds"))
        {
            if (SceneManager.GetActiveScene().name != "scene_main")
            {
                bannerView.Destroy();
            }
        }
    }
}
