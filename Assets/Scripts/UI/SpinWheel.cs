﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SpinWheel : MonoBehaviour
{
    public List<int> prize;
    public List<AnimationCurve> animationCurves;

    private bool spinning;
    private float anglePerItem;
    private int randomTime;
    private int itemNumber;

    public Button WOFbutton;
    public Text PrizeText;
    public GameObject coinpileanim;

    void Start()
    {
        spinning = false;
        anglePerItem = 360 / prize.Count;
        WOFbutton.onClick.AddListener(Update2);
        PrizeText.text = "";
    }

    void Update2()
    {
        if (!spinning)
        {

            randomTime = Random.Range(1, 4);
            itemNumber = Random.Range(0, prize.Count);
            float maxAngle = 360 * randomTime + (itemNumber * anglePerItem);
            Debug.Log(itemNumber);
            StartCoroutine(SpinTheWheel(5 * randomTime, maxAngle));
        }
    }

    IEnumerator SpinTheWheel(float time, float maxAngle)
    {
        coinpileanim.SetActive(false);
        spinning = true;

        float timer = 0.0f;
        float startAngle = transform.eulerAngles.z;
        maxAngle = maxAngle - startAngle;

        int animationCurveNumber = Random.Range(0, animationCurves.Count);
        Debug.Log("Animation Curve No. : " + animationCurveNumber);

        while (timer < time)
        {
            //to calculate rotation
            float angle = maxAngle * animationCurves[animationCurveNumber].Evaluate(timer / time);
            transform.eulerAngles = new Vector3(0.0f, 0.0f, angle + startAngle);
            timer += Time.deltaTime;
            yield return 0;
        }

        transform.eulerAngles = new Vector3(0.0f, 0.0f, maxAngle + startAngle);
        spinning = false;
        GetPrize(prize[itemNumber]);      
    }

    void GetPrize(int toAdd)
    {
        Debug.Log("Prize: " + toAdd);//use prize[itemNumnber] as per requirement
        PrizeText.text = "You won " + toAdd + " Gold !";
        coinpileanim.SetActive(true);
        coinpileanim.GetComponent<Animator>().Play("coinPile");
        PlayerPrefs.SetInt("inventory.gold", PlayerPrefs.GetInt("inventory.gold") + toAdd);
        if(toAdd == 725)
        {
            GameObject.Find("AdsManager").GetComponent<AdsManager>().ShowRewAd();
        }
    }
   
}
