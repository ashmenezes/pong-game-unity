﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.SceneManagement;

public class DebugManager : MonoBehaviour {

    private static string state;

    [Header("Set to true to always see Debug Menu.")]
    public bool SeeDebugPanel = false;

    [Header("Set to true to see Debug.Log Messages")]
    public bool SeeDebugLogs = false;
    static bool DebugLogs = false;

    [Header("Label GameObject References")]
    public GameObject DebugUI;
    public Text MasterClientCheck;
    public Text GameModeCheck;
    public Text Ping;
    public Text lvl;
    public Text PlayerCount;
    public Text RoomName;
    public Text Status;
    public Text MaxPlayers;

    public float maxSpeed = 0f;
    public float minSpeed = 0f;
    public float speed = 0f;

    /// <summary>
    /// scene_mode: 0 = main_menu (default), 
    /// scene_mode: 1 = scene_main, 
    /// scene_mode: 2 = wheel of fortune scene
    /// </summary>
    int scene_mode = 0;

    void Awake()
    {
        string scene = SceneManager.GetActiveScene().name;
        if (scene == "scene_main")
        {
            scene_mode = 1;
        }
    }

    public void resetPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
    }

    // Update is called once per frame
    void Update () {
        DebugLogs = SeeDebugLogs;
        if (Input.GetKey(KeyCode.Q) || SeeDebugPanel == true)
        {
            DebugUI.SetActive(true);
            MasterClientCheck.text = "isMasterClient: " + PhotonNetwork.isMasterClient;
            GameModeCheck.text = "Game Mode: " + DataScenes.gameMode;
            Ping.text = "Ping: " + PhotonNetwork.networkingPeer.RoundTripTime;
            Status.text = "Status: " + state;
            RoomName.text = "TouchPos: " + (Input.mousePosition.y < Screen.currentResolution.height / 2 ? (Input.mousePosition.x.ToString() + ", " + Input.mousePosition.y.ToString()) : "");
            if (scene_mode == 1 && PhotonNetwork.inRoom)
            {
                PlayerCount.text = "PlayerCount: " + PhotonNetwork.room.PlayerCount;
                RoomName.text = "RoomName: " + PhotonNetwork.room.Name;
                lvl.text = "lvl: " + PhotonNetwork.room.CustomProperties["lvl"];
                MaxPlayers.text = "MaxPlayers: " + PhotonNetwork.room.MaxPlayers;
            }
        }
        else{
            DebugUI.SetActive(false);
        }
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.LeftAlt) && Input.GetKey(KeyCode.F))
        {
            resetPlayerPrefs();
        }
    }

    public static void updateState(string s){
        Log(s);
        state = s;
    }

    public static void Log(string s)
    {
        if (DebugLogs)
            Debug.Log(s);
    }

    public void Add100Gold()
    {
        PlayerPrefs.SetInt("inventory.gold", PlayerPrefs.GetInt("inventory.gold") + 10000);
    }

    public void Add100Diamond()
    {
        PlayerPrefs.SetInt("inventory.diamond", PlayerPrefs.GetInt("inventory.diamond") + 10000);
    }

    public void resetGold()
    {
        PlayerPrefs.SetInt("inventory.gold", 0);
    }

    public void resetDiamond()
    {
        PlayerPrefs.SetInt("inventory.diamond", 0);
    }

}
