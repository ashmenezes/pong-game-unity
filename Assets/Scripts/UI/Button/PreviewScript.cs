﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PreviewScript : MonoBehaviour {

	// Use this for initialization
	void Awake () {

        GameObject.Find("PreviewBackground").GetComponent<Image>().sprite = Resources.Load<Sprite>("Backgrounds/" + PlayerPrefs.GetString("background"));
        GameObject.Find("PreviewPaddle").GetComponent<Image>().sprite = Resources.Load<Sprite>("Paddles/" + PlayerPrefs.GetString("paddle"));
        GameObject.Find("PreviewBall").GetComponent<Image>().sprite = Resources.Load<Sprite>("Balls/" + PlayerPrefs.GetString("ball"));

        if (PlayerPrefs.GetString("paddle") == "")
        { // if playerprefs string is empty
            GameObject.Find("PreviewPaddle").GetComponent<Image>().sprite = Resources.Load<Sprite>("Paddles/" + "Blue");
            PlayerPrefs.SetString("paddle", "Blue"); // assign default paddle as playerprefs string
        }

        if (PlayerPrefs.GetString("background") == "")
        {
            GameObject.Find("PreviewBackground").GetComponent<Image>().sprite = Resources.Load<Sprite>("Backgrounds/" + "desert_summer 1");
            PlayerPrefs.SetString("background", "desert_summer 1");
        }

        if (PlayerPrefs.GetString("ball") == "")
        {
            GameObject.Find("PreviewBall").GetComponent<Image>().sprite = Resources.Load<Sprite>("Balls/" + "Neon");
            PlayerPrefs.SetString("ball", "Neon");
        }

	}
}
