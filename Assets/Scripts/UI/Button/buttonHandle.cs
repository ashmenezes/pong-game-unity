﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class buttonHandle : MonoBehaviour {

	public GameObject difficulty;
	public GameObject gameModes;
	public GameObject pausePanel;
	public GameObject countDown;
    public GameObject countDownPause;
    public GameObject Ball;
	public GameObject Paddle1;
	public GameObject Paddle2;
	public Vector2 ballVelocity;
	public float ballAngular;
	public GameObject pausebutton;
    public GameObject homebutton;
    public GameObject storeButton;
    public GameObject userButton;
    public GameObject wheelButton;

    void Start()
	{
		difficulty = GameObject.Find("DifficultySelect");
		gameModes = GameObject.Find("GameModes");
		pausePanel = GameObject.Find("PausePanel");
		//Ball = GameObject.Find("Ball");
		Paddle1 = GameObject.Find("Player");
		Paddle2 = GameObject.Find("Opponent");
		pausebutton = GameObject.Find("PauseButton");
        homebutton = GameObject.Find("HomeButton");
        storeButton = GameObject.Find("BuyNoAdsButton");
        userButton = GameObject.Find("openUserPanelButton");
        wheelButton = GameObject.Find("Wheel_Button");

        if (DataScenes.gameMode == 0 || DataScenes.gameMode == 2)
        {
            if(homebutton)
                homebutton.SetActive(false);
        }
        else if (DataScenes.gameMode == 1 || DataScenes.gameMode == 3)
        {
            if(pausebutton)
                pausebutton.SetActive(false);
        }

        if (difficulty) {
			difficulty.SetActive (false);
		}
		if (pausePanel) {
			pausePanel.SetActive (false);
		}
		if (pausebutton || homebutton) {
			StartCoroutine(pauseWait());
		}
	}

    void Update()
    {
        if (countDown)
        { if (countDown.activeSelf)
            {
                if(pausebutton)
                    pausebutton.GetComponent<Button>().interactable = false;
                if(homebutton)
                   homebutton.GetComponent<Button>().interactable = false;
            }
            else
            {
                if (pausebutton)
                    pausebutton.GetComponent<Button>().interactable = true;
                if(homebutton)
                    homebutton.GetComponent<Button>().interactable = true;
            }
        }
        
    }

	public void pvp()
	{
        GameObject.Find("AdsManager").GetComponent<AdsManager>().DestroyAds();
        //DataScenes.IsItWall = false;
        DataScenes.gameMode = 1;
		SceneManager.LoadScene("scene_main");
	}

	public void pve()
	{
        //DisableCustomizeButtons();
		gameModes.SetActive (false);
        DataScenes.gameMode = 0;
        difficulty.SetActive (true);
	}

	public void pveTypes(int difficultyForAI)
	{
        GameObject.Find("AdsManager").GetComponent<AdsManager>().DestroyAds();
        //DataScenes.IsItWall = false;
        DataScenes.AIdifficulty = difficultyForAI;
		SceneManager.LoadScene("scene_main");
	}
		
	public void pvw()
	{
		//DataScenes.IsItWall = true;
		//SceneManager.LoadScene("scene_wallplay");
	}

	public void backB()
    {
        //DisableCustomizeButtons();
        difficulty.SetActive(false);
        gameModes.SetActive(true);
	}

	public void mainMenuB()
	{
        Time.timeScale = 1;
        GameObject.Find("AdsManager").GetComponent<AdsManager>().DestroyAds();
        if (DataScenes.gameMode == 0 || DataScenes.gameMode == 2)
		    SceneManager.LoadScene("main_menu");
        if(DataScenes.gameMode == 1 || DataScenes.gameMode == 3)
            GameObject.Find("NetworkManager").GetComponent<NetworkManager>().playerDisconnect();
    }

    public void arcadeMode()
    {
        //DisableCustomizeButtons();
        gameModes.SetActive(false);
        DataScenes.gameMode = 2;
        difficulty.SetActive(true);
    }

    public void tornyMode()
    {
        DataScenes.gameMode = 4;
        SceneManager.LoadScene("scene_main");
    }

    public void arcadeOnline()
    {
        GameObject.Find("AdsManager").GetComponent<AdsManager>().DestroyAds();
        DataScenes.gameMode = 3;
        SceneManager.LoadScene("scene_main");
    }

    public void wheelOfFortune()
    {
        GameObject.Find("AdsManager").GetComponent<AdsManager>().DestroyAds();
        SceneManager.LoadScene("Wheel_Fortune");
    }

    public void homeButton()
    {
        GameObject.Find("AdsManager").GetComponent<AdsManager>().DestroyAds();
        //PhotonNetwork.Disconnect();
        //SceneManager.LoadScene("main_menu");
        GameObject.Find("NetworkManager").GetComponent<NetworkManager>().playerDisconnect();
    }

    /*public void DisableCustomizeButtons()
    {
        if(storeButton.GetComponent<Button>().IsActive() == true && userButton.GetComponent<Button>().IsActive() == true)
        {
            storeButton.SetActive(false);
            userButton.SetActive(false);
            wheelButton.SetActive(false);
        }
        else if(storeButton.GetComponent<Button>().IsActive() == false && userButton.GetComponent<Button>().IsActive() == false)
        {
            storeButton.SetActive(true);
            userButton.SetActive(true);
            wheelButton.SetActive(true);
        }
    }*/

    public void pauseB()
    {
        Time.timeScale = 0;
        /*Ball = GameObject.FindGameObjectWithTag("Ball");
		countDown.SetActive(false);
		pausePanel.SetActive (true);
		Paddle1.GetComponent<MoveSlide>().enabled = false;
		//Paddle2.GetComponent<MoveSlide>().enabled = false;
		ballVelocity = Ball.GetComponent<Rigidbody2D>().velocity;
		ballAngular = Ball.GetComponent<Rigidbody2D>().angularVelocity;
		Ball.GetComponent<Rigidbody2D>().angularVelocity = 0;
		Ball.GetComponent<Rigidbody2D>().velocity = Vector2.zero;*/
	}

	public void resumeB()
	{
		//pausePanel.SetActive (false);
        //StartCoroutine(Unpause());
        countDownPause.SetActive(true);
    }

	public void playAgainB()
	{
        if (DataScenes.gameMode == 0 || DataScenes.gameMode == 2)
            GameObject.Find("Canvas").SendMessage("RestartGame", null, SendMessageOptions.RequireReceiver);
        if (DataScenes.gameMode == 1 || DataScenes.gameMode == 3)
            GameObject.Find("NetworkManager").GetComponent<NetworkManager>().playAgain();
    }


    public void BuyNoAds()
    {
        Debug.Log("Button pressed");
        IAPManagerScript.Instance.BuyNoAds();
    }

    public void resizetest()
    {
        GameObject.Find("Canvas").GetComponent<GameManager>().ResizeForBanner();
    }

    IEnumerator Unpause()
    {
        yield return new WaitForSeconds(4.5f);
        /*Ball = GameObject.FindGameObjectWithTag("Ball");
        countDown.SetActive(true);
        pausebutton.GetComponent<Button>().interactable = false;
        countDown.SetActive(false);
        pausebutton.GetComponent<Button>().interactable = true;
        Ball.GetComponent<Rigidbody2D>().velocity = ballVelocity;
        Ball.GetComponent<Rigidbody2D>().angularVelocity = ballAngular;
        Paddle1.GetComponent<MoveSlide>().enabled = true;
        Paddle2.GetComponent<MoveSlide>().enabled = true;
        Time.timeScale = 1;*/
    }

	IEnumerator pauseWait()
	{
        if (pausebutton)
            pausebutton.GetComponent<Button>().interactable = false;
        if (homebutton)
            homebutton.GetComponent<Button>().interactable = false;
		yield return new WaitForSeconds(5);
        if (pausebutton)
            pausebutton.GetComponent<Button>().interactable = true;
        if (homebutton)
            homebutton.GetComponent<Button>().interactable = true;
    }

	/*public void PauseReset()
	{
		StartCoroutine(pauseWait());
	}*/
}
