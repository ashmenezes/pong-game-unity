﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextFade : MonoBehaviour
{

	Text text;
	public float speed;
	public Color lerpedColor;

	public Color bright;
	public Color dim;

	void Start() {
		text = GetComponent<Text> ();
		lerpedColor = text.color;
	}

	// can ignore the update, it's just to make the coroutines get called for example
	void Update()
	{
		lerpedColor = Color.Lerp(bright, dim, Mathf.PingPong(Time.time, speed));
		text.color = lerpedColor;
		/*if (Input.GetKeyDown(KeyCode.Q))
		{
			StartCoroutine(FadeTextToFullAlpha(speed, text));
		}
		if (Input.GetKeyDown(KeyCode.E))
		{
			StartCoroutine(FadeTextToZeroAlpha(speed, text));
		}*/
	}

	public IEnumerator FadeTextToFullAlpha(float t, Text i)
	{
		i.color = new Color(i.color.r, i.color.g, i.color.b, 0);
		while (i.color.a < 1.0f)
		{
			i.color = new Color(i.color.r, i.color.g, i.color.b, i.color.a + (Time.deltaTime / t));
			yield return null;
		}
	}

	public IEnumerator FadeTextToZeroAlpha(float t, Text i)
	{
		i.color = new Color(i.color.r, i.color.g, i.color.b, 1);
		while (i.color.a > 0.0f)
		{
			i.color = new Color(i.color.r, i.color.g, i.color.b, i.color.a - (Time.deltaTime / t));
			yield return null;
		}
	}
}