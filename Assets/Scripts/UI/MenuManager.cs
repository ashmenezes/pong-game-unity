﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MenuManager : MonoBehaviour {

    [Header("Panels")]
    public GameObject enterUserNamePanel;

    [Header("Buttons")]
    public Button customizeUsername;

    public float timeForAd;

    void Awake()
    {
        if (PlayerPrefs.HasKey("NoAds"))
        {
            GameObject.Find("BuyNoAdsButton").SetActive(false);
        }
        GameObject.Find("GoldAmount").GetComponent<Text>().text = "gold: " + PlayerPrefs.GetInt("inventory.gold");
    }

    // Use this for initialization
    void Start ()
    {
        GameObject.Find("GoldAmount").GetComponent<Text>().text = "gold: " + PlayerPrefs.GetInt("inventory.gold");
        if (PlayerPrefs.GetString("username") == "")
        {
            enterUserNamePanel.SetActive(true);
            customizeUsername.transform.Find("Text").GetComponent<Text>().text = "Username";
        }
        customizeUsername.transform.Find("Text").GetComponent<Text>().text = PlayerPrefs.GetString("username");
        //ResizeForBanner();
    }

    public void ResizeForBanner()
    {
        GameObject positionStore = GameObject.Find("openStorePanelPos");
        positionStore.GetComponent<RectTransform>().localPosition = new Vector3(0, -47.7f, 0);
        GameObject store = GameObject.Find("StorePanel");
        store.GetComponent<RectTransform>().localScale = new Vector3(1, 0.92f, 1);

        GameObject positionUser = GameObject.Find("openUserPanelPos");
        positionUser.GetComponent<Transform>().localPosition = new Vector3(0, -47.7f, 0);
        GameObject user = GameObject.Find("UserPanel");
        user.GetComponent<RectTransform>().localScale = new Vector3(1, 0.92f, 1);
    }

    public void setUsername(UnityEngine.UI.Text text)
    {
        PlayerPrefs.SetString("username", text.text);
        customizeUsername.transform.Find("Text").GetComponent<Text>().text = text.text;
        enterUserNamePanel.SetActive(false);
    }

    public void editUserName()
    {
        enterUserNamePanel.SetActive(true);
        enterUserNamePanel.transform.Find("InputField").transform.Find("Text").GetComponent<Text>().text = PlayerPrefs.GetString("username");
    }
}
