﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;


public class ListController : MonoBehaviour {

    public int selection;

    [Header("List of items")]
    public Object[] ItemImages;
	public GameObject ContentPanel;
	public GameObject ListPaddlePrefab;
	public GameObject ListBackgroundPrefab;
	public GameObject ListBallPrefab;

    [Header("Purchasing Panels")]
    public GameObject confirmPurchasePanel;
    public GameObject purchaseFailure;

    private int paddleCost = 1500;
    private int backgroundCost = 1500;
    private int ballCost = 1500;

	ArrayList images;

	void Start () {
		repopulateList (selection);
	}

	void setPreviewPaddle(Sprite sprite){
        DebugManager.Log("Equipping Paddle");
        GameObject.Find("PreviewPaddle").GetComponent<Image> ().sprite = sprite;
		PlayerPrefs.SetString ("paddle", sprite.name);
	}

	void setPreviewBackground(Sprite sprite){
        DebugManager.Log("Equipping Background");
        GameObject.Find("PreviewBackground").GetComponent<Image> ().sprite = sprite;
		PlayerPrefs.SetString ("background", sprite.name);
	}

    void setPreviewBall(Sprite sprite){
        DebugManager.Log("Equipping Ball");
        GameObject.Find("PreviewBall").GetComponent<Image>().sprite = sprite;
        PlayerPrefs.SetString("ball", sprite.name);
    }

    void buyPaddle(Button button, Sprite sprite)
    {
        if (PlayerPrefs.GetInt("inventory.gold") - paddleCost < 0){
            DebugManager.Log("Purchasing Paddle Failed!");
            confirmPurchasePanel.SetActive(false);
            purchaseFailure.SetActive(true);
            purchaseFailure.transform.Find("areyousure").GetComponent<Text>().text = "You don't have " + paddleCost + "g to make this purchase!";
        }else{
            DebugManager.Log("Purchasing Paddle Successful!");
            PlayerPrefs.SetString(sprite.name, "y");
            PlayerPrefs.SetInt("inventory.gold", PlayerPrefs.GetInt("inventory.gold") - paddleCost);
            confirmPurchasePanel.SetActive(false);
            initEquipButton(button);
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(delegate { setPreviewPaddle(sprite); });
            setPreviewPaddle(sprite);
            GameObject.Find("GoldAmount").GetComponent<Text>().text = "gold: " + PlayerPrefs.GetInt("inventory.gold");
        }
    }

    void buyBackground(Button button, Sprite sprite)
    {
        if (PlayerPrefs.GetInt("inventory.gold") - backgroundCost < 0){
            DebugManager.Log("Purchasing Background Failed!");
            confirmPurchasePanel.SetActive(false);
            purchaseFailure.SetActive(true);
            purchaseFailure.transform.Find("areyousure").GetComponent<Text>().text = "You don't have " + backgroundCost + "g to make this purchase!";
        }
        else{
            DebugManager.Log("Purchasing Background Successful!");
            PlayerPrefs.SetString(sprite.name, "y");
            PlayerPrefs.SetInt("inventory.gold", PlayerPrefs.GetInt("inventory.gold") - backgroundCost);
            confirmPurchasePanel.SetActive(false);
            initEquipButton(button);
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(delegate { setPreviewBackground(sprite); });
            setPreviewBackground(sprite);
            GameObject.Find("GoldAmount").GetComponent<Text>().text = "gold: " + PlayerPrefs.GetInt("inventory.gold");
        }
    }

    void buyBall(Button button, Sprite sprite)
    {
        if (PlayerPrefs.GetInt("inventory.gold") - ballCost < 0){
            DebugManager.Log("Purchasing Ball Failed!");
            confirmPurchasePanel.SetActive(false);
            purchaseFailure.SetActive(true);
            purchaseFailure.transform.Find("areyousure").GetComponent<Text>().text = "You don't have " + ballCost + "g to make this purchase!";
        }
        else{
            DebugManager.Log("Purchasing Ball Successful!");
            PlayerPrefs.SetString(sprite.name, "y");
            PlayerPrefs.SetInt("inventory.gold", PlayerPrefs.GetInt("inventory.gold") - ballCost);
            confirmPurchasePanel.SetActive(false);
            initEquipButton(button);
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(delegate { setPreviewBall(sprite); });
            setPreviewBall(sprite);
            GameObject.Find("GoldAmount").GetComponent<Text>().text = "gold: " + PlayerPrefs.GetInt("inventory.gold");
        }
    }

	void clearAllItems() {
		var children = new List<GameObject>();
		foreach (Transform child in ContentPanel.transform) children.Add(child.gameObject);
		children.ForEach(child => Destroy(child));
	}

    void initBuyButton(Button button)
    {
        button.transform.Find("Text").GetComponent<Text>().text = "Buy";
        ColorBlock cb = button.colors;
        cb.normalColor = new Color(4f / 255f, 173f / 255f, 26f / 255f);
        cb.highlightedColor = new Color(6f / 255f, 143f / 255f, 24f / 255f);
        cb.pressedColor = new Color(6f / 255f, 143f / 255f, 24f / 255f);
        button.colors = cb;
    }

    void initEquipButton(Button button)
    {
        button.transform.Find("Text").GetComponent<Text>().text = "Equip";
        ColorBlock cb = button.colors;
        cb.normalColor = new Color(59f / 255f, 114f / 255f, 244f / 255f);
        cb.highlightedColor = new Color(45f / 255f, 100f / 255f, 165f / 255f);
        cb.pressedColor = new Color(45f / 255f, 100f / 255f, 165f / 255f);
        button.colors = cb;
    }

    void showConfirmPurchasePanel(ListItemController controller, CosmeticItem item)
    {
        confirmPurchasePanel.SetActive(true);
        Button yes = confirmPurchasePanel.transform.Find("Yes").GetComponent<Button>();
        yes.onClick.RemoveAllListeners();
        if (selection == 1){ // Paddle
            DebugManager.Log("Purchasing Paddle");
            confirmPurchasePanel.transform.Find("areyousure").GetComponent<Text>().text = "Are you sure you want to purchase " + item.Icon.name + " for " + paddleCost + "G?";
            yes.onClick.AddListener(delegate { buyPaddle(controller.button, item.Icon); });
        }
        else if (selection == 2){ // Background
            DebugManager.Log("Purchasing Background");
            confirmPurchasePanel.transform.Find("areyousure").GetComponent<Text>().text = "Are you sure you want to purchase " + item.Icon.name + " for " + backgroundCost + "G?";
            yes.onClick.AddListener(delegate { buyBackground(controller.button, item.Icon); });
        }
        else if (selection == 3){ // Ball
            DebugManager.Log("Purchasing Ball");
            confirmPurchasePanel.transform.Find("areyousure").GetComponent<Text>().text = "Are you sure you want to purchase " + item.Icon.name + " for " + ballCost + "G?";
            yes.onClick.AddListener(delegate { buyBall(controller.button, item.Icon); });
        }
        
    }

	public void repopulateList(int s) {
		clearAllItems ();
		selection = s;

		GameObject prefab;
		Object[] ItemImages;

		if (selection == 1) {
			ItemImages = Resources.LoadAll ("Paddles", typeof(Sprite));
			prefab = ListPaddlePrefab; 
			ContentPanel.GetComponent<HorizontalLayoutGroup> ().spacing = 10;
		} else if (selection == 2) {
			ItemImages = Resources.LoadAll ("Backgrounds", typeof(Sprite));
			prefab = ListBackgroundPrefab; 
			ContentPanel.GetComponent<HorizontalLayoutGroup> ().spacing = 10;
		} else if (selection == 3) {
			ItemImages = Resources.LoadAll ("Balls", typeof(Sprite));
			prefab = ListBallPrefab; 
			ContentPanel.GetComponent<HorizontalLayoutGroup> ().spacing = 10;
		} else
			return;

		images = new ArrayList ();

		int count = 0;
		foreach (Sprite i in ItemImages) {
			images.Add (new CosmeticItem (i, i.name));
			count += 1;
		}

		foreach (CosmeticItem item in images) {

			GameObject newCos = Instantiate (prefab) as GameObject;

			ListItemController controller = newCos.GetComponent<ListItemController> ();

			controller.Icon.sprite = item.Icon;
			controller.Name.text = item.Name;

            if (selection == 1){ // If in the Paddle menu
                if (PlayerPrefs.HasKey(item.Icon.name)){ // equipping
                    initEquipButton(controller.button);
                    controller.button.onClick.RemoveAllListeners();
                    controller.button.onClick.AddListener(delegate { setPreviewPaddle(item.Icon); });
                }else{ // buying
                    initBuyButton(controller.button);
                    controller.button.onClick.RemoveAllListeners();
                    controller.button.onClick.AddListener(delegate { showConfirmPurchasePanel(controller, item); });
                }
            }
            else if (selection == 2){ // If in the Background menu
                if (PlayerPrefs.HasKey(item.Icon.name)){ // equipping
                    initEquipButton(controller.button);
                    controller.button.onClick.RemoveAllListeners();
                    controller.button.onClick.AddListener(delegate { setPreviewBackground(item.Icon); });
                }else{ // buying
                    initBuyButton(controller.button);
                    controller.button.onClick.RemoveAllListeners();
                    controller.button.onClick.AddListener(delegate { showConfirmPurchasePanel(controller, item); });
                }
            }else if (selection == 3){ // If in the Ball menu
                if (PlayerPrefs.HasKey(item.Icon.name)){ // equipping
                    initEquipButton(controller.button);
                    controller.button.onClick.RemoveAllListeners();
                    controller.button.onClick.AddListener(delegate { setPreviewBall(item.Icon); });
                }else{ // buying
                    initBuyButton(controller.button);
                    controller.button.onClick.RemoveAllListeners();
                    controller.button.onClick.AddListener(delegate { showConfirmPurchasePanel(controller, item); });
                }
            }
			newCos.transform.SetParent (ContentPanel.transform);
			newCos.transform.localScale = Vector3.one;
		}

	}
}
