﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CosmeticItem {

	public Sprite Icon;
	public string Name;

	public CosmeticItem (Sprite icon, string name){
		Icon = icon;
		Name = name;
	}
}
