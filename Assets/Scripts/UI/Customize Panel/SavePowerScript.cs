﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;

public class SavePowerScript : MonoBehaviour
{

    private void Start()
    {
        if (gameObject.name != "SavePowerUp(Clone)")
        {
            // Black, blue, green, orange, Pink, Purple, Red, turquoise, white, yellow
            int SpriteChoose = Random.Range(0, 8);
            switch (SpriteChoose)
            {
                case 0:
                    GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Paddles/Black");
                    break;
                case 1:
                    GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Paddles/Blue");
                    break;
                case 2:
                    GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Paddles/Green");
                    break;
                case 3:
                    GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Paddles/Orange");
                    break;
                case 4:
                    GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Paddles/Pink");
                    break;
                case 5:
                    GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Paddles/Purple");
                    break;
                case 6:
                    GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Paddles/Red");
                    break;
                case 7:
                    GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Paddles/Turquoise");
                    break;
                case 8:
                    GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Paddles/Yellow");
                    break;
                default:
                    break;
            }
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Ball")
        {
            Regex rgx = new Regex("(Brick)[0-9]");
            if (DataScenes.gameMode == 3 && PhotonNetwork.isMasterClient)
            {
                if (rgx.IsMatch(gameObject.name))
                {
                    GetComponent<PhotonView>().RpcSecure("DestroyBrick", PhotonTargets.All, true, gameObject.name);
                }
                else if (gameObject.name == "SavePowerUp(Clone)")
                {
                    PhotonNetwork.Destroy(gameObject);
                }
            }
            else if (DataScenes.gameMode == 2)
                Destroy(gameObject);
        }
    }

    [PunRPC]
    void DestroyBrick(string name)
    {
        Destroy(GameObject.Find(name));
    }
}