﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class TouchZoneAnimate : MonoBehaviour {

	public Color lerpedColor;
	public float speed;

	private Color startColor;
	private bool pause = false;

	// Use this for initialization
	void Start () {
		startColor = GetComponent<Image> ().color;
		lerpedColor = startColor;
	}

	// Update is called once per frame
	void Update () {
		if (pause == false) {
			lerpedColor = Color.Lerp (
				new Color (1f, 1f, 1f, 1f),
				new Color (1f, 1f, 1f, 0.85f),
				Mathf.PingPong (Time.time, speed));
			GetComponent<Image> ().color = lerpedColor;
		}
	}

	public void Score() {
		pause = true;
		GetComponent<Image> ().color = startColor;
		for (int i = 0; i < 10; i++) {
			StartCoroutine(BlinkOff (0.25f));
			StartCoroutine(BlinkOn (0.25f));
		}
		StartCoroutine(BlinkOff (1f));
		StartCoroutine(BlinkOn (1f));
		pause = false;
	}

	IEnumerator BlinkOff(float time) {
		Color tmp = GetComponent<Image>().color;
		tmp = new Color (1f, 1f, 1f, 0f);
		GetComponent<Image>().color = tmp;
		yield return new WaitForSeconds(time);
	}

	IEnumerator BlinkOn(float time) {
		yield return new WaitForSeconds(time);
		Color tmp = GetComponent<Image>().color;
		tmp = new Color (1f, 1f, 1f, 1f);
		GetComponent<Image>().color = tmp;
	}
}
