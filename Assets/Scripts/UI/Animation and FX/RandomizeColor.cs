﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RandomizeColor : MonoBehaviour {

	public int number;

	// Use this for initialization
	public void randomize () {
		if (SceneManager.GetActiveScene ().name == "scene_main" || SceneManager.GetActiveScene ().name == "scene_wallplay") {
			string background = GameObject.Find ("Background").GetComponent<RandomBackground> ().selectedBackground.name;
			if (background == "Neon-Basketball-trans")
				gameObject.GetComponent<Renderer> ().material.color = new Color (5, 0, 0);
			else if (background == "Neon-football-trans")
				gameObject.GetComponent<Renderer> ().material.color = new Color (0, 2, 0);
			else if (background == "Neon-icehockey-trans")
				gameObject.GetComponent<Renderer> ().material.color = new Color (0, 0, 1);
		} else {
			number = Random.Range (0, 3);
			if (number == 0) {
				gameObject.GetComponent<Renderer> ().material.color = new Color (5, 0, 0);
			} else if (number == 1) {
				gameObject.GetComponent<Renderer> ().material.color = new Color (0, 2, 0);
			} else if (number == 2) {
				gameObject.GetComponent<Renderer> ().material.color = new Color (0, 0, 1);
			} else {
				Debug.Log ("Unassigned color" + number);
			} 
		}
	}
}
