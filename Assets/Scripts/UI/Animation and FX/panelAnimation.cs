﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class panelAnimation : MonoBehaviour {

	public bool panelIsOpen = false;
	public GameObject closePos;
	public GameObject openPos;

	Vector3 dest;

	public void activatePanel(bool value) {
		panelIsOpen = value;
	}
	
	// Update is called once per frame
	void Update () {		
		if (panelIsOpen == true)
			dest = openPos.transform.position;
		else
			dest = closePos.transform.position;
		gameObject.GetComponent<RectTransform>().position = Vector3.Lerp(transform.position, dest, 0.5f);		
		HideIfClickedOutside(gameObject);
	}

	private void HideIfClickedOutside(GameObject panel) {
		if (Input.GetMouseButton(0) && panel.activeSelf && 
			!RectTransformUtility.RectangleContainsScreenPoint(
				panel.GetComponent<RectTransform>(), 
				Input.mousePosition, 
				null)) {
			activatePanel(false);
		}
	}
}
