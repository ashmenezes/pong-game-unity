﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeonAnimate : MonoBehaviour {

	public Color lerpedColor;
	public float speed;

	private Color startColor;
	private bool pause = false;

	// Use this for initialization
	void Start () {
		startColor = GetComponent<SpriteRenderer> ().color;
		lerpedColor = startColor;
	}
	
	// Update is called once per frame
	void Update () {
		if (pause == false) {
			lerpedColor = Color.Lerp (
				new Color (1f, 1f, 1f, 1f),
				new Color (1f, 1f, 1f, 0.7f),
				Mathf.PingPong (Time.time, speed));
			GetComponent<SpriteRenderer> ().color = lerpedColor;
		}
	}

	public void Score() {
		pause = true;
		GetComponent<SpriteRenderer> ().color = startColor;
		for (int i = 0; i < 10; i++) {
			StartCoroutine(BlinkOff (0.25f));
			StartCoroutine(BlinkOn (0.25f));
		}
		StartCoroutine(BlinkOff (1f));
		StartCoroutine(BlinkOn (1f));
		pause = false;
	}

	IEnumerator BlinkOff(float time) {
		Color tmp = GetComponent<SpriteRenderer>().color;
		tmp = new Color (1f, 1f, 1f, 0f);
		GetComponent<SpriteRenderer>().color = tmp;
		yield return new WaitForSeconds(time);
	}

	IEnumerator BlinkOn(float time) {
		yield return new WaitForSeconds(time);
		Color tmp = GetComponent<SpriteRenderer>().color;
		tmp = new Color (1f, 1f, 1f, 1f);
		GetComponent<SpriteRenderer>().color = tmp;
	}
}
