﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomBackground : MonoBehaviour {

	public Sprite[] images = new Sprite[3];
	public Sprite selectedBackground;
    public GameObject background;


	void Start () 
	{
        if (DataScenes.gameMode == 0)
        {
            int num = UnityEngine.Random.Range(0, images.Length);
            GetComponent<SpriteRenderer>().sprite = images[num];
            selectedBackground = images[num];
            GameObject.Find("LeftBackground").GetComponent<SpriteRenderer>().sprite = selectedBackground;
            //GameObject.Find ("Space Background").GetComponent<RandomizeColor> ().randomize();
        }
    }
}
