﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundScript : MonoBehaviour {

    private VolumeManager music;
    public UnityEngine.UI.Toggle musictogglebutton;

	// Use this for initialization
	void Start () {
        music = GameObject.FindObjectOfType<VolumeManager>();
        UpdateI();
	}
    

    // Update is called once per frame
    public void PauseMusic () {
        music.ToggleSound();
        UpdateI();
	}



    private void UpdateI()
    {
        if (PlayerPrefs.GetInt("Muted", 0) == 0)
        {
            AudioListener.volume = 1;

        }
        else
        {
            AudioListener.volume = 0;
        }
    }
}
    
