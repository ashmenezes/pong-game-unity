﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallMove : Photon.MonoBehaviour {

	public float startSpeed;
	public float speed;
	public /*static*/ GameObject countDown;

	public float maxSpeed;
	public float acc;

	public float easymaxSpeed;
	public float mediummaxSpeed;
	public float hardmaxSpeed;

	public float easyAcc;
	public float mediumAcc;
	public float hardAcc;

    public string WhoHitLast;

	private ParticleSystem particles;
	private AudioSource SlideBounce;
	private AudioSource WallBounce;
	private AudioSource Explosion;

	/*public float lerpSmoothing = 5f;

	bool isAlive = true;
	Vector3 position;
	Quaternion rotation;*/


    [Header("Lerp to real position")]
	public Vector2 realPosition = Vector3.zero;
	public Quaternion realRotation = Quaternion.identity;
	public float lerpSmoothing = 1f;
    public Vector3 positionAtLastPacket = Vector3.zero;
    public double currentTime = 0.0;
    public double currentPacketTime = 0.0;
    public double lastPacketTime = 0.0;
    public double timeToReachGoal = 0.0;

    /*[Header("Extrapolation")]
    [Range(0.9f, 1.1f)]
    public float Factor = 0.98f;    // this factor makes the extrapolated movement a bit slower. the idea is to compensate some of the lag-dependent variance.
    // some internal values. see comments below
    private Vector3 latestCorrectPos = Vector3.zero;
    private Vector3 movementVector = Vector3.zero;
    private Vector3 errorVector = Vector3.zero;
    private double lastTime = 0;*/

    [Header("Jobin's solution")]
    Vector2 dir;

    /*void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
		if (stream.isWriting) {
			stream.SendNext (transform.position);
		} else {
			transform.position = (Vector3) stream.ReceiveNext ();
		}
	}

	IEnumerator Alive() {
		while (isAlive) {
			transform.position = Vector3.Lerp (transform.position, position, Time.deltaTime * lerpSmoothing);

			yield return null;
		}
	}*/

    void Awake() {
		GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite>("Balls/" + PlayerPrefs.GetString ("ball"));
		if (PlayerPrefs.GetString ("ball") == "") {
			GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> ("Balls/" + "Neon");
			PlayerPrefs.SetString ("ball", "Neon");
		}
	}

	void Start()
	{
        PhotonNetwork.sendRate = 50;
        PhotonNetwork.sendRateOnSerialize = 25;

        //if (DataScenes.gameMode == 1) {
        //	StartCoroutine ("Alive");
        //}

        particles = GameObject.Find ("particle").GetComponent<ParticleSystem>();
		SlideBounce = GameObject.Find ("BounceSlideSound").GetComponent<AudioSource> ();
		WallBounce = GameObject.Find ("BounceWallSound").GetComponent<AudioSource> ();
		Explosion = GameObject.Find ("Explosion").GetComponent<AudioSource> ();
		speed = startSpeed;

		//GetComponent<Renderer>().enabled = false;
		countDown = GameObject.Find ("Countdown");
		//countDown.SetActive(false);
		Invoke ("BallGo", 4f);
		GameObject.Find("Canvas").GetComponent<GameManager>().startCountdown();

        if (DataScenes.gameMode == 0 || DataScenes.gameMode == 2)
        {
            if (DataScenes.AIdifficulty == 0) {
			    maxSpeed = easymaxSpeed;
			    acc = easyAcc;
		    } else if (DataScenes.AIdifficulty == 1) {
			    maxSpeed = mediummaxSpeed;
			    acc = mediumAcc;
		    } else if (DataScenes.AIdifficulty == 2) {
			    maxSpeed = hardmaxSpeed;
			    acc = hardAcc;
		    }
        }
        else if (DataScenes.gameMode == 1 || DataScenes.gameMode == 3)
        {
            maxSpeed = 40f;
            acc = 0.5f;
        }
    }

	[PunRPC]
	public void playExplosion() {
		particles.Play ();
		particles.GetComponent<Transform>().position = GetComponent<Transform> ().position;
	}

	float hitFactor(Vector2 ballPos, Vector2 racketPos, float racketHeight) 
	{
		return (ballPos.y - racketPos.y) / racketHeight;
	}
		
	void OnCollisionEnter2D (Collision2D col)
	{
        /*if (DataScenes.gameMode == 1 && GetComponent<PhotonView>().isMine)
        {
            int id = PhotonNetwork.player.ID;
            foreach (PhotonPlayer p in PhotonNetwork.playerList){ // finds the player that isn't you
                if (p.ID != PhotonNetwork.player.ID)
                    id = p.ID;
            }
            GetComponent<PhotonView>().TransferOwnership(id); // transfers ownership to that player
        }*/

        /*----------------------------------WHO TOUCHED THE BALL LAST----------------------------------*/

        if (((DataScenes.gameMode == 3 && PhotonNetwork.isMasterClient) || DataScenes.gameMode == 2) && (col.gameObject.name == "Player" || col.gameObject.name == "Opponent"))
        {
            WhoHitLast = col.gameObject.name;
            //Debug.Log(WhoHitLast + " hit last");
        }

        /*---------------------------------------------------------------------------------------------*/

        if (col.gameObject.name == "SlideLeft" || col.gameObject.name == "SlideRight")
		{
			SlideBounce.Play ();
		}
		if(col.gameObject.name == "TopWall" || col.gameObject.name == "BottomWall")
		{
			WallBounce.Play ();
		}
		if(col.gameObject.name == "LeftWall" || col.gameObject.name == "RightWall")
        {
            Explosion.Play ();
            playExplosion();
        }
        /*
		if (col.gameObject.transform.position.x == GameObject.Find("Clone_SpawnPoint").transform.position.x && col.gameObject.tag == "Player")
		{
            Debug.Log("Left");
            float y = hitFactor(transform.position, col.transform.position, col.collider.bounds.size.y);
            Vector2 dir = new Vector2(1, y).normalized;
			onBallBounce (y, dir);
			if (DataScenes.gameMode == 1) {
				GetComponent<PhotonView> ().RpcSecure("onBallBounce", PhotonTargets.Others, true, y, dir);
			}
		}

		if (col.gameObject.transform.position.x == GameObject.Find("SpawnPoint").transform.position.x && col.gameObject.tag == "Player")
		{
            Debug.Log("Right");
            float y = hitFactor(transform.position, col.transform.position, col.collider.bounds.size.y);
            Vector2 dir = new Vector2(-1, y).normalized;
			onBallBounce (y, dir);
			if (DataScenes.gameMode == 1) {
				GetComponent<PhotonView> ().RPC ("onBallBounce", PhotonTargets.Others, y, dir);
			}
		}
        */

        // Ball angle bounce new code
        /*if (col.gameObject.tag == "Player")
        {
            Vector2 v = GetComponent<Rigidbody2D>().velocity;
            v.Normalize();
            float dotOfvn = Vector2.Dot(v, (Vector2) transform.up);
            Vector2 R = new Vector2();
            R += -2 * dotOfvn * (Vector2) transform.up + v;

            RaycastHit2D hit = Physics2D.Raycast(new Vector2(this.transform.position.x,
                    this.transform.position.y), GetComponent<Rigidbody2D>().velocity);

            Vector2 vectorFromHitPointToPadCenter =
                hit.point - (Vector2) transform.position;

            float length = Vector2.Dot(vectorFromHitPointToPadCenter, transform.right);

            var percentageOfLengthVSHalfPadLength = length / (col.collider.bounds.size.y / 2);

            R += (Vector2)transform.right * percentageOfLengthVSHalfPadLength +
                (1 - percentageOfLengthVSHalfPadLength) * (Vector2)transform.up;
            R.Normalize();

            GetComponent<Rigidbody2D>().velocity = R * speed;
        }*/

        if (col.gameObject.name == "Opponent")
        {
            float y = hitFactor(transform.position, col.transform.position, col.collider.bounds.size.y);
            Vector2 dir = new Vector2(1, y).normalized;
            onBallBounce(y, dir);
        }

        if (col.gameObject.name == "Player")
        {
            float y = hitFactor(transform.position, col.transform.position, col.collider.bounds.size.y);
            Vector2 dir = new Vector2(-1, y).normalized;
            onBallBounce(y, dir);
        }
    }

	[PunRPC]
	void onBallBounce (float y, Vector2 dir) {
		GetComponent<Rigidbody2D>().velocity = dir * speed;
		if ((speed + acc) < maxSpeed)
			speed = speed + acc;
	}

	public void InvokeBallGo(float time){
		Invoke ("BallGo", time);
	}

	void BallGo()
	{
        Vector2 orientation = GameObject.Find("Canvas").GetComponent<GameManager>().orientation;
		GetComponent<Rigidbody2D>().velocity = orientation * speed;
	}

	public void BallEndGame()
	{
        DebugManager.Log("BallEndGame");
		particles.GetComponent<Transform>().position = GetComponent<Transform> ().position;
		particles.Play ();
        if (DataScenes.gameMode == 0 || DataScenes.gameMode == 2)
            Destroy(gameObject);
		else if ((DataScenes.gameMode == 1 || DataScenes.gameMode == 3) && PhotonNetwork.isMasterClient)
            PhotonNetwork.Destroy(gameObject);
	}

	void Update() {
        // Interpolation
        if ((DataScenes.gameMode == 1 || DataScenes.gameMode == 3) && !photonView.isMine)
		{   
            // Stack Overflow Predict Movement of Master ball
            if (!photonView.isMine)
            {
                timeToReachGoal = currentPacketTime - lastPacketTime;
                //lerpSmoothing = Vector3.Distance(positionAtLastPacket, realPosition) * 0.1f;
                currentTime += Time.deltaTime;
                transform.position = Vector2.Lerp(positionAtLastPacket, realPosition, (float)(currentTime / timeToReachGoal));
                transform.rotation = Quaternion.Lerp(transform.rotation, realRotation, Time.deltaTime * 10f);
            }

            // OG movement
            //transform.position = Vector3.Lerp (transform.position, realPosition, Time.deltaTime * 7f);
            //transform.position = realPosition;
            //rigidBody2D.velocity = realVel;
			//rigidBody2D.angularVelocity = realAngVel;
			//transform.rotation = Quaternion.Lerp (transform.rotation, realRotation, Time.deltaTime * 10f);
        }


        
        
        // Extrapolation
        /*
        if (photonView.isMine)
        {
            return; // if this object is under our control, we don't need to apply received position-updates 
        }

        // we move the object, based on the movement it did between the last two updates.
        // in addition, we factor in the error between the last correct update we got and our extrapolated position at that time.
        transform.localPosition += (this.movementVector + this.errorVector) * this.Factor * Time.deltaTime;

        //Lerp + extrapolation
        //transform.localPosition = Vector3.Lerp(transform.localPosition, transform.localPosition += (this.movementVector + this.errorVector), this.Factor * Time.deltaTime);
        //rigidBody2D.velocity = Vector2.Lerp(rigidBody2D.velocity, rigidBody2D.velocity += (this.movementVectorVel + this.errorVectorVel), this.Factor * Time.deltaTime);
        */





        //Jobin's suggestion
        /*if (!photonView.isMine)
        {
            Vector3.RotateTowards(transform.position, dir, 1f, 0.0f);
        }*/
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
        // OG method
        if (stream.isWriting && photonView.isMine)
		{
            stream.SendNext( (Vector2) transform.position);
            stream.SendNext( (Quaternion) transform.rotation);

        }
        else
		{
            realPosition = (Vector2) stream.ReceiveNext();
            realRotation = (Quaternion) stream.ReceiveNext ();
            currentTime = 0.0;
            positionAtLastPacket = transform.position;
            lastPacketTime = currentPacketTime;
            currentPacketTime = info.timestamp;
        }
        



        //Extrapolation
        /*
        if (stream.isWriting)
        {
            // the controlling player of a cube sends only the position
            Vector3 pos = transform.localPosition;
            stream.Serialize(ref pos);
        }
        else
        {
            // other players (not controlling this cube) will read the position and timing and calculate everything else based on this
            Vector3 updatedLocalPos = Vector3.zero;
            stream.Serialize(ref updatedLocalPos);

            double timeDiffOfUpdates = info.timestamp - this.lastTime;  // the time that passed after the sender sent it's previous update
            this.lastTime = info.timestamp;


            // the movementVector calculates how far the "original" cube moved since it sent the last update.
            // we calculate this based on the sender's timing, so we exclude network lag. that makes our movement smoother.
            this.movementVector = (updatedLocalPos - this.latestCorrectPos) / (float)timeDiffOfUpdates;

            // the errorVector is how far our cube is away from the incoming position update. using this corrects errors somewhat, until the next update arrives.
            // with this, we don't have to correct our cube's position with a new update (which introduces visible, hard stuttering).
            this.errorVector = (updatedLocalPos - transform.localPosition) / (float)timeDiffOfUpdates;

            // next time we get an update, we need this update's position:
            this.latestCorrectPos = updatedLocalPos;
        }
        */




        // Jobin's suggestion
        /*if (stream.isWriting)
        {
            Vector2 dir = rigidBody2D.velocity.normalized;
            stream.Serialize(ref dir);
        }
        else if (stream.isReading)
        {
            dir = Vector2.zero;
            stream.Serialize(ref dir);
        }*/
        
    }

}
