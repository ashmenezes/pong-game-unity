﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveSlide : MonoBehaviour {

	public float speed = 30;
	public string axis;
	public float vel;
	public bool player2 = false;

	private Vector2 pos;

	void Start() {
		if (PlayerPrefs.GetString ("paddle") == "") { // if playerprefs string is empty
			GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> ("Paddles/" + "Blue"); // Use default skin
			PlayerPrefs.SetString ("paddle", "Blue"); // assign default paddle as playerprefs string
		} else {
			GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Paddles/" + PlayerPrefs.GetString("paddle")); // Load sprite for paddle
		}
	}

	void FixedUpdate () 
	{
		Move ();
	}

	public void Move (){
        #if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER
            Vector3 mouseLocation = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(transform.position.x, mouseLocation.y, transform.position.z);
            //vel = Input.GetAxisRaw (axis);
            //GetComponent<Rigidbody2D>().velocity = new Vector2(0, vel) * speed;
        #else
			if (Input.touchCount > 0) {
				//Vector3 touchpos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
				//GameObject.Find("TextForTest").GetComponent<Text>().text = touchpos.x.ToString() + touchpos.y.ToString();
				if (Input.touchCount > 1) {
					Touch touch1 = Input.GetTouch(0);
					Touch touch2 = Input.GetTouch(1);
					Vector3 touchedPos1 = Camera.main.ScreenToWorldPoint(touch1.position);
					Vector3 touchedPos2 = Camera.main.ScreenToWorldPoint(touch2.position);
					if (touchedPos1.x > 24.5f && touchedPos1.y > -14.5f && touchedPos1.y < 15f && player2 == false)
						TouchControl(0);
					if (touchedPos2.x > 24.5f && touchedPos2.y > -14.5f && touchedPos2.y < 15f && player2 == false)
						TouchControl(1);
					else if (touchedPos1.x < -24.5f && touchedPos1.y > -14.5f && touchedPos1.y < 15f && player2 == true && GameObject.Find("SlideLeft").GetComponent<AI>().enabled == false)
						TouchControl(0);
					else if (touchedPos2.x < -24.5f && touchedPos2.y > -14.5f && touchedPos2.y < 15f && player2 == true && GameObject.Find("SlideLeft").GetComponent<AI>().enabled == false)
						TouchControl(1);
				} else {
					Touch touch = Input.GetTouch(0);
					Vector3 touchedPos = Camera.main.ScreenToWorldPoint(touch.position);
                    if (touch.position.x > Mathf.RoundToInt(Display.main.systemHeight * 0.5f) && player2 == false)
						TouchControl(0);
					else if (touchedPos.x < -24.5f && touchedPos.y > -14.5f && touchedPos.y < 15f && player2 == true && GameObject.Find("SlideLeft").GetComponent<AI>().enabled == false)
						TouchControl(0);
				}
			}
#endif
    }

    public void TouchControl(int touchnumber) {
        Debug.Log("touchcontrol");
		Touch touch = Input.GetTouch(touchnumber); // get first touch since touch count is greater than zero
		if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved) {
			Vector3 touchedPos = Camera.main.ScreenToWorldPoint(touch.position); // get the touch position from the screen touch to world point
			Vector3 dest = new Vector3(transform.position.x, touchedPos.y, transform.position.z);
			if (dest.y < 12 && dest.y > -12)
				transform.position = Vector3.Lerp(transform.position, dest, 1); // lerp and set the position of the current object to that of the touch, but smoothly over time.
		}
	}

	public void Move (float v){
		vel = v;
		GetComponent<Rigidbody2D>().velocity = new Vector2(0, vel) * speed;
	}

}
