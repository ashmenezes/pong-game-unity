﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NetworkPlayer : Photon.MonoBehaviour {

	public float lerpSmoothing = 1f;

    public Vector3 positionAtLastPacket = Vector3.zero;
    public double currentTime = 0.0;
    public double currentPacketTime = 0.0;
    public double lastPacketTime = 0.0;
    public double timeToReachGoal = 0.0;

    [Header("Extrapolation")]
    [Range(0.9f, 1.1f)]
    public float Factor = 0.98f;    // this factor makes the extrapolated movement a bit slower. the idea is to compensate some of the lag-dependent variance.

    // some internal values. see comments below
    private Vector3 latestCorrectPos = Vector3.zero;
    public Vector3 movementVector = Vector3.zero;
    private Vector3 errorVector = Vector3.zero;
    private double lastTime = 0;
    Vector3 previousUpdateValue = Vector3.zero;

    BoxCollider2D boxCol;

    // Use this for initialization
    void Start () {
		if (DataScenes.gameMode == 1 || DataScenes.gameMode == 3 || DataScenes.gameMode == 4) {
            if (!PhotonNetwork.isMasterClient && photonView.isMine) {
                DebugManager.Log("Calling updateInfoMaster from Client Player");
                GetComponent<PhotonView>().RpcSecure("updateInfoMaster", PhotonTargets.Others, true, PlayerPrefs.GetString("username"), PlayerPrefs.GetString("paddle"), PlayerPrefs.GetString("background"));
            }
            if (photonView.isMine) { // If gameObject is controller paddle
                if (!PhotonNetwork.isMasterClient) {
					GameObject.Find ("Main Camera").transform.Rotate (0, 0, 180, Space.Self);
					GameObject.Find ("Backgrounds").transform.Rotate (0, 0, 180, Space.Self);
                    GetComponent<PhotonView>().RpcSecure("SpawnBall", PhotonTargets.MasterClient, true);
                    GetComponent<MoveSlide>().enabled = true;
                }
                gameObject.name = "Player"; // Rename opponent paddle
            } else {
				gameObject.name = "Opponent"; // Rename opponent paddle
				GetComponent<MoveSlide> ().enabled = false; // disable controls for opponent paddle
			}
		}
        boxCol = GetComponent<BoxCollider2D>();
	}

    [PunRPC]
    void updateInfoMaster(string username, string paddle, string background)
    {
        if (!photonView.isMine)
        {
            DebugManager.Log("UpdateInfoMaster from Opponent: " + GetComponent<PhotonView>().viewID);
            GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Paddles/" + paddle); // Updates paddle skin
            GameObject.Find("LeftBackground").GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Backgrounds/" + background); // Updates background skin
            GameObject.Find("PlayerUserName").GetComponent<Text>().text = PlayerPrefs.GetString("username"); // Updates player username
            GameObject.Find("OpponentUserName").GetComponent<Text>().text = username; // Updates opponent username
            GameObject.Find("Player").GetComponent<NetworkPlayer>().updateInfoMaster(username, paddle, background);
        }
        else
            DebugManager.Log("UpdateInfoMaster from Player: " + GetComponent<PhotonView>().viewID);
            GetComponent<PhotonView>().RpcSecure("updateInfo", PhotonTargets.Others, true, PlayerPrefs.GetString("username"), PlayerPrefs.GetString("paddle"), PlayerPrefs.GetString("background"));
    }

    [PunRPC]
    void updateInfo(string username, string paddle, string background)
    {
        if (!photonView.isMine)
        {
            DebugManager.Log("updateInfo from Opponent: " + GetComponent<PhotonView>().viewID);
            GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Paddles/" + paddle);  // Updates paddle skin
            GameObject.Find("LeftBackground").GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Backgrounds/" + background); // Updates background skin
            GameObject.Find("PlayerUserName").GetComponent<Text>().text = PlayerPrefs.GetString("username"); // Updates player username
            GameObject.Find("OpponentUserName").GetComponent<Text>().text = username; // Updates opponent username
        }
    }

    [PunRPC]
    public void SpawnBall()
    {
        Transform BallSpawn = GameObject.Find("BallSpawn").transform;
        PhotonNetwork.Instantiate("Prefabs/MultipleScenes/Game/Ball",
            BallSpawn.position,
            BallSpawn.rotation,
            0);
    }

    void Update () {
        // Stack Overflow Predict Movement of Master ball
        /*if (!photonView.isMine)
        {
            timeToReachGoal = currentPacketTime - lastPacketTime;
            currentTime += Time.deltaTime * 0.3f;
            transform.position = Vector2.Lerp(positionAtLastPacket, realPosition, (float)(currentTime / timeToReachGoal));
        }*/

        // OG Lerp Movement
        /*if (!photonView.isMine) {
			transform.position = Vector3.Lerp (transform.position, realPosition, Time.deltaTime * 2);
			transform.rotation = Quaternion.Lerp (transform.rotation, realRotation, Time.deltaTime * 2);
		}*/

        // Extrapolation
        if (photonView.isMine)
        {
            return; // if this object is under our control, we don't need to apply received position-updates 
        }

        // we move the object, based on the movement it did between the last two updates.
        // in addition, we factor in the error between the last correct update we got and our extrapolated position at that time.
        Vector3 currentUpdateValue = (this.movementVector + this.errorVector) * this.Factor;
        if (previousUpdateValue.y == 0f)
            previousUpdateValue = currentUpdateValue;
        else if (currentUpdateValue.y != previousUpdateValue.y) // if changes found then update
            transform.localPosition += currentUpdateValue * Time.deltaTime;
        previousUpdateValue = currentUpdateValue;

        if (PhotonNetwork.isMasterClient)
        {
            /*float offSet = (movementVector.y * 0.0003f) - 0.5f; // 0.0003
            if (offSet > 0f)
                boxCol.offset = new Vector2(boxCol.offset.x, offSet);
            float size = (boxCol.size.y) + (offSet * 2f); // default size.y = 10, now 12
            if (size > 10f)
                boxCol.size = new Vector2(boxCol.size.x, size);*/

            float size = (movementVector.y * 0.0074f) + 12f;
            boxCol.size = new Vector2(boxCol.size.x, size);

        }
    }

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {

        // Stack Overflow Lerp movement
        /*
        if (stream.isWriting && photonView.isMine)
        {
            stream.SendNext((Vector2)transform.position);
            //stream.SendNext( (Quaternion) transform.rotation);
            //stream.SendNext( (Vector2) GetComponent<Rigidbody2D>().velocity);
            //stream.SendNext( (float) GetComponent<Rigidbody2D>().angularVelocity);
            //stream.SendNext((bool)hasHitSurface);

        }
        else
        {
            realPosition = (Vector2)stream.ReceiveNext();
            //realRotation = (Quaternion) stream.ReceiveNext ();
            //realVel = (Vector2) stream.ReceiveNext();
            //realAngVel = (float) stream.ReceiveNext();
            //hasHitSurface = (bool)stream.ReceiveNext();
            currentTime = 0.0;
            positionAtLastPacket = transform.position;
            lastPacketTime = currentPacketTime;
            currentPacketTime = info.timestamp;
        }
        */

        // OG Lerp movement
        /*if (stream.isWriting) {
			stream.SendNext (transform.position);
			stream.SendNext (transform.rotation);
		} else {
			realPosition = (Vector3) stream.ReceiveNext ();
			realRotation = (Quaternion) stream.ReceiveNext ();
		}*/

        //Extrapolation
        if (stream.isWriting)
        {
            // the controlling player of a cube sends only the position
            Vector3 pos = transform.localPosition;
            stream.Serialize(ref pos);

            this.movementVector = (Vector3.zero - this.latestCorrectPos) / (float)(info.timestamp - this.lastTime);
        }
        else
        {
            // other players (not controlling this cube) will read the position and timing and calculate everything else based on this
            Vector3 updatedLocalPos = Vector3.zero;
            stream.Serialize(ref updatedLocalPos);

            double timeDiffOfUpdates = info.timestamp - this.lastTime;  // the time that passed after the sender sent it's previous update
            this.lastTime = info.timestamp;


            // the movementVector calculates how far the "original" cube moved since it sent the last update.
            // we calculate this based on the sender's timing, so we exclude network lag. that makes our movement smoother.
            this.movementVector = (updatedLocalPos - this.latestCorrectPos) / (float)timeDiffOfUpdates;

            // the errorVector is how far our cube is away from the incoming position update. using this corrects errors somewhat, until the next update arrives.
            // with this, we don't have to correct our cube's position with a new update (which introduces visible, hard stuttering).
            this.errorVector = (updatedLocalPos - transform.localPosition) / (float)timeDiffOfUpdates;

            // next time we get an update, we need this update's position:
            this.latestCorrectPos = updatedLocalPos;
        }
    }

    public void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        if (otherPlayer.UserId != PhotonNetwork.player.UserId && GameObject.Find("Canvas").GetComponent<GameManager>().hasGameEnded == false)
            GameObject.Find("NetworkManager").GetComponent<NetworkManager>().opponentDisconnect(otherPlayer);
    }
}
