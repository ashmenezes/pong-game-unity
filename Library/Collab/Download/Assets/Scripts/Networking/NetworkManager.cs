﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;

public class NetworkManager : MonoBehaviour {

    public class RoomSave
    {
        public string roomName;
        public RoomOptions roomOptions;
    }

	private string version = "v0.1";
    public RoomOptions roomOptions;

    public string MasterPlayerPrefabName;
    public string ClientPlayerPrefabName;
    public Transform playerSpawnPoint;
	public Transform cloneSpawnPoint;
	public int numberOfRooms;
    public GameObject connectingPanel;
    public GameObject opponentLeftPanel;
    public GameObject disconnectingPanel;
    public GameObject tournyPanel;

    // Tournament variables
    RoomInfo[] roomsList;
    bool receivedRoomList = false;
    int retryCounter = 0;
    RoomOptions tournyRoom;
    RoomOptions tournyDupRoom;
    RoomSave roomSave;
    string[] rankings = new string[7];

    // GUIDE FOR USING custom room property "lvl" IN TOURNAMENT MODE
    // lvl = 0: First match in first room hasn't started yet, or
    //          First match in second room hasn't started yet.
    // lvl = 1: First match in first room has started/is ongoing, or
    //          First match in second room has started/is ongoing.
    // lvl = 2: Second match in second room hasn't started yet.
    // lvl = 3: Second match in second room has started/is ongoing.
    // lvl = 4: Second match in second room has ended.

    // Use this for initialization
    void Start () {
        DebugManager.Log("NetworkManager Start");

        hideAllPopUpPanels();
        showConnecting(true);
        PhotonNetwork.ConnectUsingSettings (version);
        PhotonNetwork.autoCleanUpPlayerObjects = true;

		roomOptions = new RoomOptions () { PublishUserId = true, IsVisible = true, MaxPlayers = 2};

        // Create tournyRoom room type
        tournyRoom = new RoomOptions() { PublishUserId = true, IsVisible = true, MaxPlayers = 4 };
        tournyRoom.CustomRoomPropertiesForLobby = new string[] { "lvl" };
        tournyRoom.CustomRoomProperties = new ExitGames.Client.Photon.Hashtable() {
            { "lvl", 0 }
        };

        tournyDupRoom = new RoomOptions() { PublishUserId = true, IsVisible = false, MaxPlayers = 2 };
        tournyDupRoom.CustomRoomPropertiesForLobby = new string[] { "lvl", "tournyRoomName"};
        tournyDupRoom.CustomRoomProperties = new ExitGames.Client.Photon.Hashtable() {
            { "lvl", 0 },
            { "tournyRoomName", ""},
        };

        RoomSave roomSave = new RoomSave();
    }

    void OnConnectedToMaster()
    {
        DebugManager.updateState("OnConnectedToMaster()");
        PhotonNetwork.JoinLobby(TypedLobby.Default);
    }

    void OnJoinedLobby(){
		DebugManager.updateState ("OnJoinedLobby()");
		if (DataScenes.gameMode == 4) {
            StartCoroutine("FindTournyRoom"); // Wait for list of rooms
        }
        else
			PhotonNetwork.JoinRandomRoom ();
	}

    IEnumerator FindTournyRoom()
    {
        DebugManager.Log("FindTournyRoom()");
        while (!receivedRoomList){      // Wait for OnRecieveRoomListUpdate() to give list of rooms
            yield return new WaitForSeconds(1);
        }

        foreach (RoomInfo room in roomsList)    // For each room in room list
        {
            if (room.MaxPlayers == 4)   // if room is tournament mode
            {                       
                if (room.PlayerCount < room.MaxPlayers)     // if room isn't full
                {
                    if ((int)room.CustomProperties["lvl"] == 0)    // if match hasn't started
                    {
                        roomSave.roomName = room.Name;  // save room information

                        PhotonNetwork.JoinRoom(roomSave.roomName);
                        yield break;
                    }
                }
            }
        }
        createAndJoinNewTourny();
    }

    void createAndJoinNewTourny(){                                              // Create new tournament room
        DebugManager.Log("createAndJoinNewTourny()");

        name = "";                                                              // Create random string for room name
        var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".ToCharArray();
        while (name.Length < 15)
            name += characters[Random.Range(0, characters.Length)];

        roomSave.roomName = name;
        roomSave.roomOptions = tournyRoom;
        PhotonNetwork.CreateRoom(roomSave.roomName, roomSave.roomOptions, TypedLobby.Default);        // Create and join room
    }

	public void CreateRandomRoom() {
        DebugManager.updateState ("CreateRandomRoom()");
		PhotonNetwork.CreateRoom("", roomOptions, TypedLobby.Default);
	}

	void OnJoinedRoom() {
        DebugManager.updateState("OnJoinedRoom()");
        if (DataScenes.gameMode == 4)   // if in tourny mode
        {
            if (PhotonNetwork.room.MaxPlayers == 4)    // if room is not dup
            {
                if ((int)PhotonNetwork.room.CustomProperties["lvl"] == 0)   // if game hasn't started yet
                {
                    if (PhotonNetwork.playerList.Length > 3)    // if all players have arrived
                    {
                        StartCoroutine(SendPlayersToDuplicateRoom());   // Send two players to other tourny room
                    }
                }
                else if ((int) PhotonNetwork.room.CustomProperties["lvl"] == 1)     // if first room's game is on-going
                {
                    // wait and do nothing
                }else if ((int) PhotonNetwork.room.CustomProperties["lvl"] == 2)    // if first room's game is done
                {
                    if (PhotonNetwork.playerList.Length > 1)
                    {
                        PhotonNetwork.room.IsOpen = false;
                        PhotonNetwork.room.CustomProperties["lvl"] = 3; // Second game has started!
                        GetComponent<PhotonView>().RpcSecure("StartGame", PhotonTargets.All, true); // Start Game
                    }
                    else
                    {
                        /// If 
                        /// - The game in the first room ended, and
                        /// - The winning player left, then
                        /// CURRENT PLAYER WINS TOURNY
                    }
                }
            }
            else if (PhotonNetwork.room.MaxPlayers == 2)    // if room is dup
            {
                if (PhotonNetwork.room.PlayerCount > 1)     // if all players have arrived
                {
                    GetComponent<PhotonView>().RpcSecure("StartGame", PhotonTargets.All, true);
                    PhotonNetwork.room.CustomProperties["lvl"] = 1; // Game has started!
                }
            }
        }
        else
        {
            // Do regular online stuff
            int numberOfplayers = PhotonNetwork.playerList.Length;
            if (numberOfplayers > 1)
            {
                GameObject player = PhotonNetwork.Instantiate(ClientPlayerPrefabName,
                    cloneSpawnPoint.position,
                    cloneSpawnPoint.rotation,
                    0);
                player.GetComponent<MoveSlide>().enabled = false;
                GameObject.Find("Canvas").GetComponent<GameManager>().countDown.SetActive(true);
                GetComponent<PhotonView>().RpcSecure("showConnecting", PhotonTargets.All, true, false);
            }
            else
            {
                PhotonNetwork.Instantiate(MasterPlayerPrefabName,
                    playerSpawnPoint.position,
                    playerSpawnPoint.rotation,
                    0);
                StartCoroutine(WaitInRoom());
            }
        }
	}

    IEnumerator SendPlayersToDuplicateRoom()
    {
        DebugManager.Log("SendPlayersToDuplicateRoom()");
        // Stop players from seeing this room in lobby's room list
        PhotonNetwork.room.IsVisible = false;                                   

        // Store first and second player to be sent to the other tourny room
        PhotonPlayer firstPlayer = PhotonNetwork.playerList[0];
        PhotonPlayer secondPlayer = PhotonNetwork.playerList[1];

        // Create random string for room name
        name = "";
        var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".ToCharArray();
        while (name.Length < 15)
            name += characters[Random.Range(0, characters.Length)];

        // Send first player to create room
        GetComponent<PhotonView>().RpcSecure("SendPlayerToCreateDuplicateTournyRoom", PhotonTargets.All, true, name, firstPlayer.UserId);
        yield return new WaitForSeconds(3);

        // Send second player to join that room
        GetComponent<PhotonView>().RpcSecure("SendPlayerToJoinDuplicateTournyRoom", PhotonTargets.All, true, name, secondPlayer.UserId);
        yield return new WaitForSeconds(3);

        // Start the game
        GetComponent<PhotonView>().RpcSecure("StartGame", PhotonTargets.All, true);
        PhotonNetwork.room.CustomProperties["lvl"] = 1;     // Game has started!
    }

    [PunRPC]
    void SendPlayerToCreateDuplicateTournyRoom(string name, string playerId)
    {
        DebugManager.Log("SendPlayerToCreateDuplicateTournyRoom()");
        if (PhotonNetwork.player.UserId == playerId)    // if desired player
        {
            // Save current room information
            roomSave.roomName = name;
            roomSave.roomOptions = tournyDupRoom;

            // Make sure players in dup room can return to this room
            tournyDupRoom.CustomRoomProperties["tournyRoomName"] = PhotonNetwork.room.Name;

            PhotonNetwork.LeaveRoom();
            PhotonNetwork.CreateRoom(roomSave.roomName, roomSave.roomOptions, TypedLobby.Default);
        }
    }

    [PunRPC]
    void SendPlayerToJoinDuplicateTournyRoom(string name, string playerId)
    {
        DebugManager.Log("SendPlayerToJoinDuplicateTournyRoom()");
        if (PhotonNetwork.player.UserId == playerId)    // if desired room
        {
            // Save current room info
            roomSave.roomName = name;

            PhotonNetwork.LeaveRoom();
            PhotonNetwork.JoinRoom(roomSave.roomName);
        }
    }

    [PunRPC]
    void StartGame()
    {
        DebugManager.Log("StartGame()");
        if (PhotonNetwork.isMasterClient)
        {
            PhotonNetwork.Instantiate(MasterPlayerPrefabName,
                    playerSpawnPoint.position,
                    playerSpawnPoint.rotation,
                    0);
        }
        else
        {
            GameObject player = PhotonNetwork.Instantiate(ClientPlayerPrefabName,
                    cloneSpawnPoint.position,
                    cloneSpawnPoint.rotation,
                    0);
            player.GetComponent<MoveSlide>().enabled = false;
            GameObject.Find("Canvas").GetComponent<GameManager>().countDown.SetActive(true);
            GetComponent<PhotonView>().RpcSecure("showConnecting", PhotonTargets.All, true, false);
        }
    }

    IEnumerator WaitInRoom()
    {
        DebugManager.Log("WaitInRoom()");
        int limit = 10 + Random.Range(0, 10);
        int timer = 0;
        bool interrupt = false;
        while (timer < limit)
        {
            if (PhotonNetwork.playerList.Length > 1)
            {
                interrupt = true;
                break;
            }
            yield return new WaitForSeconds(1);
            timer += 1;
        }
        if (interrupt)
            yield break;
        leaveRoomAndReconnect();
    }

	void OnPhotonRandomJoinFailed() {
        DebugManager.updateState ("OnPhotonRandomJoinFailed()");
	    CreateRandomRoom ();
	}

    void OnPhotonJoinRoomFailed()
    {
        DebugManager.Log("OnPhotonJoinRoomFailed()");
        if (DataScenes.gameMode == 4){
            if (retryCounter < 3){
                PhotonNetwork.JoinRoom(roomSave.roomName);
                retryCounter++;
                DebugManager.Log("Join Tourny Retry #" + retryCounter + " (" + (3 - retryCounter) + ") tries left");
            }
            else{
                DebugManager.Log("Join Tourny Retry Limit Reached (3). Exiting to Main Menu.");
                playerDisconnect();
                SceneManager.LoadScene("main_menu");
            }
        }
    }

	void OnPhotonCreateRoomFailed() {
        DebugManager.updateState ("OnPhotonCreateRoomFailed()");
        if (DataScenes.gameMode == 4)
        {
            if (retryCounter < 3){                                          // NOTE: Does not distinguish between creating a new tourny or a duplicate room
                retryCounter++;
                DebugManager.Log("Create Tourny Retry " + retryCounter);
                PhotonNetwork.CreateRoom(roomSave.roomName, roomSave.roomOptions, TypedLobby.Default);
            }
            else
            {
                DebugManager.Log("Create Tourny Retry Limit Reached (3). Exiting to Main Menu.");
                playerDisconnect();
                SceneManager.LoadScene("main_menu");
            }
        }
        else
        {
            PhotonNetwork.JoinRandomRoom();
        }
	}

    public void hideAllPopUpPanels()
    {
        showConnecting(false);
        opponentLeftPanel.SetActive(false);
        disconnectingPanel.SetActive(false);
    }

    [PunRPC]
    public void showConnecting(bool val)
    {
        connectingPanel.SetActive(val);
    }

    public void endGame()
    {
        DebugManager.Log("endGame()");
        GameObject.Find("Canvas").GetComponent<GameManager>().hasGameEnded = true;
        PhotonNetwork.DestroyAll();
        PhotonNetwork.Disconnect();
    }

    public void playAgain()
    {
        DebugManager.Log("playAgain()");
        hideAllPopUpPanels();
        connectingPanel.SetActive(true);
        GameObject.Find("GameOverPanel").SetActive(false);
        Scene s = SceneManager.GetActiveScene();
        SceneManager.LoadScene(s.buildIndex);
    }

    public void leaveRoomAndReconnect()
    {
        DebugManager.Log("leaveRoomAndReconnect");
        PhotonNetwork.DestroyAll();
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.LeaveLobby();
        PhotonNetwork.JoinLobby(TypedLobby.Default);
    }

    public void playerDisconnect()
    {
        DebugManager.Log("playerDisconnect");
        hideAllPopUpPanels();
        disconnectingPanel.SetActive(true);
        PhotonNetwork.DestroyAll();
        PhotonNetwork.Disconnect();
        SceneManager.LoadScene("main_menu");
    }

    public void opponentDisconnect(PhotonPlayer otherPlayer)
    {
        DebugManager.Log("opponentDisconnect()");
        DebugManager.Log("opponentDisconnect(otherPlayer)");
        PhotonNetwork.DestroyAll();
        PhotonNetwork.Disconnect();
        StartCoroutine(ShowDisconnectMessage());
    }

    IEnumerator ShowDisconnectMessage()
    {
        DebugManager.Log("ShowDisconnectMessage()");
        opponentLeftPanel.SetActive(true);
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("main_menu");
    }

    void OnReceivedRoomListUpdate()
    {
        roomsList = PhotonNetwork.GetRoomList();
        receivedRoomList = true;
    }

    public void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        DebugManager.Log("OnPhotonPlayerDisconnected()");
        if (DataScenes.gameMode == 4 && otherPlayer.UserId != PhotonNetwork.player.UserId)      // if disconnecting player is not me
        {
            GameManager gm = GameObject.Find("Canvas").GetComponent<GameManager>();
            gm.GameOverPanel.SetActive(true);
            gm.EndRightScore.text = "WINNER";
            gm.EndLeftScore.text = "LOSER";
            try   // If there is a ball, destroy it
            {
                PhotonNetwork.Destroy(GameObject.FindGameObjectWithTag("Ball"));
            }
            catch { }
        }
        else
        {
            playerDisconnect();
        }
    }
}
