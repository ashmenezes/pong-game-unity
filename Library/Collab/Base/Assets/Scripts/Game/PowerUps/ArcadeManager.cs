﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcadeManager : MonoBehaviour {

    public GameObject[] PowerUps;
    public int PowerUpsAmount;

    public Vector3 DefaultScalePlayer;
    public Vector3 DefaultScaleOpponent;


    void Start()
    {
        GetDefaultValues();
        PowerUpsAmount = 0;
        SpawnPowerUpAfter();
        SpawnPowerUpAfter();
    }

    GameObject RandomPowerup()
    {
        int PowerUpIndex = Random.Range(0, PowerUps.Length);
        return PowerUps[PowerUpIndex];
    }

    Vector3 RandomPosition()
    {
        Vector3 spawnPoint;
        spawnPoint.x = Random.Range(-10, 10);
        spawnPoint.y = Random.Range(-10, 10);
        spawnPoint.z = 0;
        return spawnPoint;
    }

    float RandomTime()
    {
        float spawnTime;
        spawnTime = 5 + Random.Range(1, 5);
        return spawnTime;
    }

    /*void SpawnPowerUp()
    {
        Instantiate(RandomPowerup(), RandomPosition(), Quaternion.identity);
    }*/

    IEnumerator SpawnPowerUp(float delayTime)
    {
        GameObject powerUpParticle = Resources.Load<GameObject>("Prefabs/arcade_scene/Game/PowerUpParticle");
        Vector3 spawnPosition = RandomPosition();
        if (DataScenes.gameMode == 3 && PhotonNetwork.isMasterClient)
        {
            var particle = PhotonNetwork.Instantiate("PowerUpParticle", spawnPosition, Quaternion.identity, 0);
            yield return new WaitForSeconds(delayTime);
            PhotonNetwork.Instantiate("RandomPowerup", spawnPosition, Quaternion.identity, 0);
            PhotonNetwork.Destroy(particle);
        }
        else{
            var particle = Instantiate(powerUpParticle, spawnPosition, Quaternion.identity);
            yield return new WaitForSeconds(delayTime);
            Instantiate(RandomPowerup(), spawnPosition, Quaternion.identity);
            Destroy(particle);
        }
    }

    public void SpawnPowerUpAfter()
    {
        if (PowerUpsAmount < 2)
        {
            float time = RandomTime();
            StartCoroutine(SpawnPowerUp(time));
            //Invoke("SpawnPowerUp", time);
            PowerUpsAmount++;
            //Debug.Log("should Spawn in " + time);
        }
    }

    void GetDefaultValues()
    {
        DefaultScalePlayer = GameObject.Find("Player").transform.localScale;
        DefaultScaleOpponent = GameObject.Find("Opponent").transform.localScale;
    }

    [PunRPC]
    public void ResetPowerUps()
    {
        GameObject.Find("Player").transform.localScale = DefaultScalePlayer;
        GameObject.Find("Opponent").transform.localScale = DefaultScaleOpponent;
        GameObject[] powers = GameObject.FindGameObjectsWithTag("PowerUp");
        foreach (GameObject todestroy in powers)
            Destroy(todestroy);
    }

}
