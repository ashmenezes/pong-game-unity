﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	private int maxscore = 5;
    public bool hasGameEnded = false;

	public /*static*/ int LeftPlayer = 0;
	public /*static*/ int RightPlayer = 0;
	public /*static*/ Text leftScore;
	public /*static*/ Text rightScore;
	public /*static*/ Text endLeftScore;
	public /*static*/ Text endRightScore;
	public /*static*/ Text EndLeftScore;
	public /*static*/ Text EndRightScore;

	public GameObject GameOverPanel;
	public GameObject pausebutton;
	public /*static*/ GameObject Ball;

    public Vector2[] directions = new Vector2[] {Vector2.down, Vector2.up};
	public Vector2 orientation;

	// Prefabs
	public GameObject BallPrefab;
	public GameObject SlideLeftPrefab;
	public GameObject SlideRightPrefab;
	public GameObject NetworkManager;

	// GameObject References
	public Transform BallSpawn;
	public Transform PlayerSpawn;
	public Transform CloneSpawn;
	public GameObject countDown;

	void Awake () {

		GameObject.Find("RightBackground").GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Backgrounds/" + PlayerPrefs.GetString("background"));

		if (PlayerPrefs.GetString("background") == "")
		{
			GameObject.Find("RightBackground").GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Backgrounds/" + "desert_summer 1");
			PlayerPrefs.SetString("background", "desert_summer 1");
		}

        EndLeftScore = GameObject.Find("EndResultLeft").GetComponent<Text>();
        EndRightScore = GameObject.Find("EndResultRight").GetComponent<Text>();
        endRightScore = GameObject.Find("EndRightScore").GetComponent<Text>();
        endLeftScore = GameObject.Find("EndLeftScore").GetComponent<Text>();

        GameOverPanel = GameObject.Find("GameOverPanel");
        if (GameOverPanel)
        {
            GameOverPanel.SetActive(false);
        }
        //ResizeForBanner();
    }

	void Start () {

		//Awake stuff
		if (DataScenes.IsItWall == false)
		{
			if (DataScenes.gameMode == 0) { // If Practice Mode or Practice Arcade Mode
				// Spawn Paddles
				GameObject slideright = Object.Instantiate (SlideRightPrefab, PlayerSpawn.position, PlayerSpawn.rotation);
				slideright.name = "Player";
				slideright.GetComponent<NetworkPlayer> ().enabled = false;

				GameObject slideleft = Object.Instantiate (SlideLeftPrefab, CloneSpawn.position, CloneSpawn.rotation);
				slideleft.name = "Opponent";
				//slideleft.GetComponent<AI>().enabled = true; // Enable AI

				Object.Instantiate (BallPrefab, BallSpawn.position, BallSpawn.rotation);
				countDown.SetActive (true);
			} else if (DataScenes.gameMode == 1 || DataScenes.gameMode == 3 || DataScenes.gameMode == 4) {
				NetworkManager.SetActive (true);
			} else if (DataScenes.gameMode == 2) {
				// Spawn Paddles
				GameObject slideright = Object.Instantiate (SlideRightPrefab, PlayerSpawn.position, PlayerSpawn.rotation);
				slideright.name = "Player";
				slideright.GetComponent<NetworkPlayer> ().enabled = false;

				GameObject slideleft = Object.Instantiate (SlideLeftPrefab, CloneSpawn.position, CloneSpawn.rotation);
				slideleft.name = "Opponent";

				Object.Instantiate (BallPrefab, BallSpawn.position, BallSpawn.rotation);
				countDown.SetActive (true);
				GetComponent<ArcadeManager> ().enabled = true;
			}
        }

		// Start stuff
		if (DataScenes.IsItWall == false)
			leftScore = GameObject.Find("LeftScore").GetComponent<Text>();
		rightScore = GameObject.Find("RightScore").GetComponent<Text>();
		pausebutton = GameObject.Find ("PauseButton");

		int num = UnityEngine.Random.Range (0, directions.Length);
		orientation = directions[num];		
	}

    public void ResizeForBanner()
    {
        GameObject cameraMain = GameObject.Find("Main Camera");
        cameraMain.GetComponent<Camera>().orthographicSize = 16.25f;
        cameraMain.GetComponent<Transform>().position = new Vector3(0,1.25f,-10);

        GameObject gameUi = GameObject.Find("Game UI");
        gameUi.GetComponent<RectTransform>().localScale = new Vector3(0.8f, 0.8f, 1);
        countDown.GetComponent<RectTransform>().localPosition = new Vector3(0, -30, 0);

        /*gameUi.GetComponent<RectTransform>().localPosition = new Vector3(0, -65, 0);

        GameObject panelUi = GameObject.Find("Panel UI");
        panelUi.GetComponent<RectTransform>().localScale = new Vector3(0.9f, 0.9f, 1);
        panelUi.GetComponent<RectTransform>().localPosition = new Vector3(0, -65, 0);

        countDown.GetComponent<RectTransform>().localScale = new Vector3(0.9f, 0.9f, 1);*/
    }

    public void startCountdown() {
        if (DataScenes.gameMode == 3 || DataScenes.gameMode == 4)
            GameObject.Find("Canvas").GetComponent<ArcadeManager>().enabled = true;
        StartCoroutine (Waitto());
	}

	public void Score(string wallName)
	{
        if (DataScenes.gameMode == 2)
            GameObject.Find("Canvas").GetComponent<ArcadeManager>().ResetPowerUps();
        else if (DataScenes.gameMode == 3)
            GetComponent<PhotonView>().RpcSecure("ResetPowerUps", PhotonTargets.All, true);
        pausebutton.GetComponent<Button>().interactable = false;
		/*if (GameObject.Find("SlideRight").GetComponent<AbilityBehaviour>() != null)
        {
            GameObject.Find("SlideRight").GetComponent<AbilityBehaviour>().ReloadBehaviour();
        }
        if (GameObject.Find("SlideLeft").GetComponent<AbilityBehaviour>() != null)
        {
            GameObject.Find("SlideLeft").GetComponent<AbilityBehaviour>().ReloadBehaviour();
        }*/
		if (DataScenes.IsItWall == true) {
			ScoreWall (wallName);
		} 
		else if (DataScenes.gameMode == 0 || ((DataScenes.gameMode == 1 || DataScenes.gameMode == 3 || DataScenes.gameMode == 4) && PhotonNetwork.isMasterClient) || DataScenes.gameMode == 2)
        {
			ScoreNormal (wallName);
		}
	}

	/*public static*/ void ScoreNormal (string wallName1)
	{
        if (wallName1 == "RightWall")
		{
			LeftPlayer++;
            leftScore.text = "" + LeftPlayer;
            endLeftScore.text = "" + LeftPlayer;
            if (DataScenes.gameMode == 1 || DataScenes.gameMode == 3 || DataScenes.gameMode == 4)
            {
                GetComponent<PhotonView>().RpcSecure("PhotonUpdateScoreRight", PhotonTargets.Others, true, LeftPlayer);
            }
            
			orientation = directions[1];
			//GameObject.Find ("Background").GetComponent<NeonAnimate> ().Score ();
			if (LeftPlayer < maxscore) {
                BallReset ();
			}
			Endgame ();
			//GameObject.Find ("ButtonHandle").GetComponent<buttonHandle> ().PauseReset();
		}

		if (wallName1 == "LeftWall")
		{
			RightPlayer++;
            rightScore.text = "" + RightPlayer;
            endRightScore.text = "" + RightPlayer;
            if (DataScenes.gameMode == 1 || DataScenes.gameMode == 3 || DataScenes.gameMode == 4)
            {
                GetComponent<PhotonView>().RpcSecure("PhotonUpdateScoreLeft", PhotonTargets.Others, true, RightPlayer);
            }

			orientation = directions[0];
			//GameObject.Find ("Background").GetComponent<NeonAnimate> ().Score ();
			if (RightPlayer < maxscore) {
                if (DataScenes.gameMode == 0 || (DataScenes.gameMode == 1 || DataScenes.gameMode == 3 || DataScenes.gameMode == 4) && GameObject.FindGameObjectWithTag("Ball").GetComponent<PhotonView>().isMine || DataScenes.gameMode == 2)
                    BallReset ();
            }
			Endgame ();
			//GameObject.Find ("ButtonHandle").GetComponent<buttonHandle> ().PauseReset();
		}
	}

    [PunRPC]
    public void PhotonUpdateScoreRight(int score)
    {
        RightPlayer = score;
        rightScore.text = "" + score;
        endRightScore.text = "" + score;
    }

    [PunRPC]
    public void PhotonUpdateScoreLeft(int score)
    {
        LeftPlayer = score;
        leftScore.text = "" + score;
        endLeftScore.text = "" + score;
    }


    /*public static*/
    void ScoreWall (string wallName2)
	{
		if (wallName2 == "LeftWall")
		{
			RightPlayer++;
			rightScore.text = "" + RightPlayer;
		}

		if (wallName2 == "RightWall")
		{
			EndRightScore.text = "GAME OVER";
			BallStop ();
			Invoke ("RestartGame", 3);
		}
	}

	public IEnumerator Waitto()
	{
		//GetComponent<Renderer>().enabled = false;
		countDown.SetActive(true);
		yield return new WaitForSeconds(4);
		countDown.SetActive(false);
		//GetComponent<Renderer>().enabled = true;
	}

	void Endgame()
	{
        if (LeftPlayer == maxscore || RightPlayer == maxscore){
            if ((DataScenes.gameMode == 1 || DataScenes.gameMode == 3 || DataScenes.gameMode == 4) && PhotonNetwork.isMasterClient)
                GetComponent<PhotonView>().RpcSecure("GameOver", PhotonTargets.All, true);
            else if (DataScenes.gameMode == 0 || DataScenes.gameMode == 2)
            {
                Ball = GameObject.FindGameObjectWithTag("Ball");
                Destroy(Ball);
                GameOver();
                GameOverPanel.SetActive(true);
            }
        }
	}

	void BallReset()
	{
        Ball = GameObject.FindGameObjectWithTag ("Ball");
		BallStop ();
		countDown.SetActive(true);
		StartCoroutine(Waitto());
		Ball.GetComponent<BallMove>().InvokeBallGo(4.5f);
	}

	void BallStop()
	{
		Ball = GameObject.FindGameObjectWithTag ("Ball");
		ParticleSystem particles = GameObject.Find ("particle").GetComponent<ParticleSystem>();
		/*rigidBody2D.velocity = Vector2.zero;
		transform.position = Vector2.zero;*/
		if (DataScenes.gameMode == 0 || DataScenes.gameMode == 2) {
			GameObject newBall = (GameObject)Instantiate (Resources.Load ("Prefabs/MultipleScenes/Game/Ball"), Vector3.zero, Quaternion.identity);
			newBall.name = "Ball(Clone)";
			//particles.GetComponent<Transform> ().position = GetComponent<Transform> ().position;
			particles.Play ();
			Destroy (Ball);
		} else if ((DataScenes.gameMode == 1 || DataScenes.gameMode == 3 || DataScenes.gameMode == 4) && GameObject.FindGameObjectWithTag("Ball").GetComponent<PhotonView>().isMine) { //&& GameObject.FindObjectOfType<NetworkPlayer>().isMaster == true) {
			//particles.GetComponent<Transform>().position = GetComponent<Transform> ().position;
			//particles.Play ();
			GameObject newBall = (GameObject) PhotonNetwork.Instantiate("Prefabs/MultipleScenes/Game/Ball", Vector3.zero, Quaternion.identity, 0);
			newBall.name = "Ball(Clone)";
			PhotonNetwork.Destroy (Ball);
			Ball.GetComponent<BallMove> ().playExplosion ();
			Ball.GetComponent<PhotonView>().RpcSecure("playExplosion", PhotonTargets.Others, true);
		}
		Ball.GetComponent<BallMove>().speed = Ball.GetComponent<BallMove>().startSpeed;
	}

    [PunRPC]
	void GameOver()
	{
        if (LeftPlayer == maxscore)
        {
            EndLeftScore.text = "WINNER";
            EndRightScore.text = "LOSER";

        }
        else if (RightPlayer == maxscore)
        {
            EndRightScore.text = "WINNER";
            EndLeftScore.text = "LOSER";
        }
        Ball = GameObject.FindGameObjectWithTag("Ball");
        Ball.GetComponent<BallMove>().BallEndGame();
        if (DataScenes.gameMode == 1 || DataScenes.gameMode == 3 || DataScenes.gameMode == 4)
            GameObject.Find("NetworkManager").GetComponent<NetworkManager>().endGame();
        GameOverPanel.SetActive(true);
    }

	void RestartGame()
	{
		Scene loadedLevel = SceneManager.GetActiveScene ();
		SceneManager.LoadScene (loadedLevel.buildIndex);
		//SceneManager.LoadScene("scene_main");
	}

    [PunRPC]
    public void QuitMatch()
    {
        foreach (GameObject g in GameObject.FindGameObjectsWithTag("Player")){
            if (g.GetComponent<PhotonView>().isMine)
                PhotonNetwork.Destroy(g);
        }
        SceneManager.LoadScene("scene_main");
    }
}
