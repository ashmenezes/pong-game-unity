﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetworkPlayer : Photon.MonoBehaviour {

	public float lerpSmoothing = 1f;

	bool isAlive = true;
	Vector3 realPosition = Vector3.zero;
	Quaternion realRotation = Quaternion.identity;

	// Use this for initialization
	void Start () {
		if (DataScenes.gameMode == 1) {
			if (photonView.isMine) { // If gameObject is controller paddle
                if (!PhotonNetwork.isMasterClient) {
					GameObject.Find ("Main Camera").transform.Rotate (0, 0, 180, Space.Self);
					GameObject.Find ("Backgrounds").transform.Rotate (0, 0, 180, Space.Self);
                    GetComponent<PhotonView>().RpcSecure("SpawnBall", PhotonTargets.MasterClient, true);
                    GetComponent<MoveSlide>().enabled = true;
                }
			} else {
				gameObject.name = "Opponent Clone"; // Rename opponent paddle
				GetComponent<MoveSlide> ().enabled = false; // disable controls for opponent paddle
			}
		}
	}

    [PunRPC]
    public void SpawnBall()
    {
        Transform BallSpawn = GameObject.Find("BallSpawn").transform;
        PhotonNetwork.Instantiate("Prefabs/MultipleScenes/Game/Ball",
            BallSpawn.position,
            BallSpawn.rotation,
            0);
    }

    void Update () {
		if (!photonView.isMine) {
			transform.position = Vector3.Lerp (transform.position, realPosition, Time.deltaTime * lerpSmoothing);
			transform.rotation = Quaternion.Lerp (transform.rotation, realRotation, Time.deltaTime * lerpSmoothing);
		}
	}

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
		if (stream.isWriting) {
			stream.SendNext (transform.position);
			stream.SendNext (transform.rotation);
		} else {
			realPosition = (Vector3) stream.ReceiveNext ();
			realRotation = (Quaternion) stream.ReceiveNext ();
		}
	}

    private void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        GameObject.Find("Canvas").GetComponent<PhotonView>().RpcSecure("QuitMatch", PhotonTargets.All, true);
    }
}
