﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetworkManager : MonoBehaviour {

	private string version = "v0.1";
	private string roomName;
	private RoomOptions roomOptions;

    public string playerPrefabName;
	public Transform playerSpawnPoint;
	public Transform cloneSpawnPoint;
	public int numberOfRooms;
    public GameObject loadingPanel;

    DebugManager dm;

	// Use this for initialization
	void Start () {
		PhotonNetwork.ConnectUsingSettings (version);
		RoomOptions roomOptions = new RoomOptions () { PublishUserId = true, isVisible = true, maxPlayers = 2};
        dm = GameObject.Find("DebugManager").GetComponent<DebugManager>();
	}

	void OnJoinedLobby(){
		dm.updateState ("Joined Lobby");
        JoinRandomRoom();
	}

	void OnConnectedToMaster(){
        dm.updateState ("Connected to Master");
		PhotonNetwork.JoinLobby (TypedLobby.Default);
	}

	public void VerifyAndEnterRoom(UnityEngine.UI.Text roomName){
        dm.updateState ("Verifying and Creating room");
		PhotonNetwork.JoinOrCreateRoom (roomName.text, roomOptions, TypedLobby.Default);
	}

	public void CreateRandomRoom() {
        dm.updateState ("Creating random room");
		PhotonNetwork.CreateRoom("", roomOptions, TypedLobby.Default);
	}

    public void JoinRandomRoom()
    {
        dm.updateState ("Joining random room");
        PhotonNetwork.JoinRandomRoom();
    }

	void OnJoinedRoom() {
        dm.updateState ("Joined room");
        loadingPanel.SetActive(false);
        int numberOfplayers = PhotonNetwork.playerList.Length;
		Debug.Log ("numberOfplayers: " + numberOfplayers);
		if (numberOfplayers > 1) {
			GameObject player = PhotonNetwork.Instantiate (playerPrefabName,
				cloneSpawnPoint.position,
				cloneSpawnPoint.rotation,
				0);
            player.GetComponent<MoveSlide>().enabled = false;
            GameObject.Find ("Canvas").GetComponent<GameManager> ().countDown.SetActive(true);
		} else {
            GameObject player = PhotonNetwork.Instantiate (playerPrefabName,
				playerSpawnPoint.position,
				playerSpawnPoint.rotation,
				0);
        }
	}

	void OnPhotonRandomJoinFailed() {
        dm.updateState ("Method: Error joining room. Room might be full");
		CreateRandomRoom ();
	}

	void OnPhotonCreateRoomFailed() {
        dm.updateState ("Method: Error creating room.");
        JoinRandomRoom();
	}
}
